<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return "lol";
});



Route::post('/tokens/create', function (Request $request) {
    $token = $request->user()->createToken($request->token_name);

    return ['token' => $token->plainTextToken];
});


Route::get('/familles',[App\Http\Controllers\Controller::class, 'ListeFamilles']);
Route::get('/articles',[App\Http\Controllers\Controller::class, 'ListeArticles']);
Route::get('/zones',[App\Http\Controllers\Controller::class, 'ListeZones']);
Route::get('/ventes',[App\Http\Controllers\Controller::class, 'ListeVentes']);

Route::get('/stocks',[App\Http\Controllers\Controller::class, 'ListeStocks']);


Route::get('/transfert/familles',[App\Http\Controllers\Controller::class, 'TransfertFamille']);
Route::get('/transfert/zones',[App\Http\Controllers\Controller::class, 'TransfertZone']);
Route::get('/transfert/articles',[App\Http\Controllers\Controller::class, 'TransfertArticle']);
Route::get('/transfert/ventes',[App\Http\Controllers\Controller::class, 'TransfertCommande']);
Route::get('/transfert/stocks',[App\Http\Controllers\Controller::class, 'TransfertStock']);




//NEW APIS
Route::get('/ventes2',[App\Http\Controllers\Controller::class, 'ListeVenteslst']);
Route::get('/ventes/planifie',[App\Http\Controllers\Controller::class, 'ListeVentesplanifie']);
Route::get('/stocks2',[App\Http\Controllers\Controller::class, 'ListeStocks2']);
Route::get('/achats',[App\Http\Controllers\Controller::class, 'ListeAchats']);
Route::get('/achats/planifie',[App\Http\Controllers\Controller::class, 'ListeAchatsplanifie']);

//NEW APIS TRansfert
Route::get('/transfert/stocks2',[App\Http\Controllers\Controller::class, 'TransfertStock2']);
Route::get('/transfert/ventes2',[App\Http\Controllers\Controller::class, 'TransfertCommande2']);
Route::get('/transfert/ventes/planifie',[App\Http\Controllers\Controller::class, 'TransfertCommandePlanifie']);
Route::get('/transfert/achats',[App\Http\Controllers\Controller::class, 'TransfertAchats']);
Route::get('/transfert/achats/planifie',[App\Http\Controllers\Controller::class, 'TransfertAchatsPlanifie']);




Route::get('/transfert/testo',[App\Http\Controllers\Controller::class, 'testo']);