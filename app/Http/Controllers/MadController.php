<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ss;

class MadController extends Controller
{
    public function createStock(Request $request)
    {
        $data = json_decode($request->data);
        if(is_array($data)){
            for($x=0;$x<count($data);$x++){
                $articleData = $data[$x];
                for($i=0;$i<count($articleData);$i++){
                    $s = Ss::where('article_id',$articleData[$i]->article_id)->where('date',$articleData[$i]->mois)->first();
                    if($s){
                        $ss = $s;
                        $ss->article_id = $articleData[$i]->article_id;
                        $ss->date = $articleData[$i]->mois;
                        $ss->stock_security = $articleData[$i]->stock_s;
                        $ss->save();    
                        
                    }else{
                        $ss = new Ss;
                        $ss->article_id = $articleData[$i]->article_id;
                        $ss->date = $articleData[$i]->mois;
                        $ss->stock_security = $articleData[$i]->stock_s;
                        $ss->save();
                    }
                }
            }
           

            return 1;
        }
       
        
    }
}
