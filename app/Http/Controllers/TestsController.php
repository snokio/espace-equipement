<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Test;
use PhpParser\Node\Expr\Cast\Array_;
use DB;

class TestsController extends Controller
{
    public function new()
    {
        for($i =0;$i<50000;$i++){
            $new = new Test;
            $new->name = "name ".$i;
            $new->save();
        }
        return "done";
        
    }
    public function get1()
    {
        $arr = Test::all();
        return $arr;
    }

    public function get2()
    {
        $json_response = collect();
        DB::table('tests')->orderBy('id')->chunk(1000, function ($users) use($json_response) {
            foreach ($users as $user) {
                $json_response->push($user);
            }
        });

        return $json_response->toJson();
    }
}
