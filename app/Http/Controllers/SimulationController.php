<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Article;

class SimulationController extends Controller
{
    public function getData(Request $request)
    {
         $Commandes = Article::when($request->article, function($q) use ($request){    //Search by Nom Article
                 $q->where('designation', 'like', '%'.$request->article.'%');
             })
         ->when($request->code, function($q) use ($request){                       //Search by Code Article
                 $q->where('code',$request->code);
             })
         ->when($request->famille, function($query) use ($request){                   //Search by Famille
                     $query->whereHas('famille', function($q) use ($request) {
                         $q->where('famille',$request->famille);
                     });
             }) 
         ->when($request->familleid, function($query) use ($request){                   //Search by Famille
                     $query->whereHas('famille', function($q) use ($request) {
                         $q->where('id',$request->familleid);
                     });
             }) 
         ->when($request->client, function($query) use ($request){                    //Search by Nom Client
                 $query->whereHas('commandes', function($q) use ($request) {
                     $q->whereHas('client', function($qi) use ($request){
                         $qi->where('designation', 'like', '%'.$request->client.'%');
                     });
                 });
             })
         ->when(($request->dateDebut && $request->dateFin), function($query) use ($request){                    //Search by Nom Client
                 $query->whereHas('commandes', function($q) use ($request) {
                     $q->whereBetween('date_vente', [$request->dateDebut, $request->dateFin]);
                 });
             })     
         ->with(['lastprevision'])      
         ->with(['commandeG'=> function($q) use ($request){
             $q->selectRaw(DB::raw('article_id,extract(MONTH from date_vente) AS semaine'))
             ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
             ->selectRaw(DB::raw('COUNT(Extract( WEEK FROM date_vente )) AS NombreVente'))
             ->selectRaw(DB::raw('COUNT(DISTINCT Extract( WEEK FROM date_vente )) AS Nombresemaine'))
             ->selectRaw(DB::raw('avg(quantite) AS MoyenneVente'))
             ->selectRaw(DB::raw('(sum(quantite)/COUNT(DISTINCT Extract( WEEK FROM date_vente ))) AS MoyennePeriode'))
             ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
             ->groupByRaw('Extract( WEEK FROM date_vente )');
                 }])
         ->with(['commandeM'=> function($q) use ($request){
             $q->selectRaw(DB::raw('article_id,extract(MONTH from date_vente) AS mois'))
             ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
             ->selectRaw(DB::raw('COUNT(Extract( MONTH FROM date_vente )) AS NombreVente'))
             ->selectRaw(DB::raw('COUNT(DISTINCT Extract( MONTH FROM date_vente )) AS NombreMois'))
             ->selectRaw(DB::raw('avg(quantite) AS MoyenneVente'))
             ->selectRaw(DB::raw('(sum(quantite)/COUNT(DISTINCT Extract( MONTH FROM date_vente ))) AS MoyennePeriode'))
             ->selectRaw(DB::raw('(sum(quantite)/COUNT(DISTINCT Extract( MONTH FROM date_vente ))) AS MoyennePeriode'))
             ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
             ->groupByRaw('article_id,Extract( MONTH FROM date_vente )');
                 }])
         ->with(['commandeD'=> function($q) use ($request){
             $q->selectRaw(DB::raw('article_id,extract(WEEK from date_vente) AS semaine'))
             ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
             ->selectRaw(DB::raw('COUNT(Extract( WEEK FROM date_vente )) AS NombreVente'))
             ->selectRaw(DB::raw('COUNT(DISTINCT Extract( WEEK FROM date_vente )) AS Nombresemaine'))
             ->selectRaw(DB::raw('avg(quantite) AS MoyenneVente'))
             ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
             
             ->whereHas('zone', function($qi) use ($request){
                         $qi->where('zone', 'like', '%'.$request->zone.'%');
                     })
             ->groupByRaw('article_id,Extract( WEEK FROM date_vente )');
                 }])
         ->with(['commandeone'=> function($q) use ($request){
             $q->selectRaw(DB::raw('article_id,extract(MONTH from date_vente) AS mois'))
             ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
             ->selectRaw(DB::raw('COUNT(Extract( MONTH FROM date_vente )) AS NombreVente'))
             ->selectRaw(DB::raw('COUNT(DISTINCT Extract( MONTH FROM date_vente )) AS NombreMois'))
             ->selectRaw(DB::raw('avg(quantite) AS MoyenneVente'))
             ->whereBetween('date_vente', [$request->dateDebut0, $request->dateFin0])
             
             ->whereHas('zone', function($qi) use ($request){
                         $qi->where('zone', 'like', '%'.$request->zone.'%');
                     })
             ->groupByRaw('article_id,Extract( MONTH FROM date_vente )');
                 }])
         ->with(['commandetwo'=> function($q) use ($request){
             $q->selectRaw(DB::raw('article_id,extract(MONTH from date_vente) AS mois'))
             ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
             ->selectRaw(DB::raw('COUNT(Extract( MONTH FROM date_vente )) AS NombreVente'))
             ->selectRaw(DB::raw('COUNT(DISTINCT Extract( MONTH FROM date_vente )) AS NombreMois'))
             ->selectRaw(DB::raw('avg(quantite) AS MoyenneVente'))
             ->whereBetween('date_vente', [$request->dateDebut1, $request->dateFin1])
             
             ->whereHas('zone', function($qi) use ($request){
                         $qi->where('zone', 'like', '%'.$request->zone.'%');
                     })
             ->groupByRaw('article_id,Extract( MONTH FROM date_vente )');
                 }])
         ->with(['commandethree'=> function($q) use ($request){
             $q->selectRaw(DB::raw('article_id,extract(MONTH from date_vente) AS mois'))
             ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
             ->selectRaw(DB::raw('COUNT(Extract( MONTH FROM date_vente )) AS NombreVente'))
             ->selectRaw(DB::raw('COUNT(DISTINCT Extract( MONTH FROM date_vente )) AS NombreMois'))
             ->selectRaw(DB::raw('avg(quantite) AS MoyenneVente'))
             ->whereBetween('date_vente', [$request->dateDebut2, $request->dateFin2])
             
             ->whereHas('zone', function($qi) use ($request){
                         $qi->where('zone', 'like', '%'.$request->zone.'%');
                     })
             ->groupByRaw('article_id,Extract( MONTH FROM date_vente )');
                 }])
         ->with(['previsions'=> function($q) use ($request){
             $q->selectRaw(DB::raw('article_id,Extract( WEEK FROM date ) as semaine,sum(prevision) as Sumprevision'))
             ->whereBetween('date', [$request->dateDebut, $request->dateFin])
             ->groupByRaw('article_id,Extract( WEEK FROM date )');
                 }])
         ->with(['previsionsm'=> function($q) use ($request){
             $q->selectRaw(DB::raw('article_id,Extract( MONTH FROM date ) as mois,sum(prevision) as Sumprevision'))
             ->whereBetween('date', [$request->dateDebut, $request->dateFin])
             ->groupByRaw('article_id,Extract( MONTH FROM date )');
                 }])
     ->with('famille')
     
     ->paginate(20);
                 return $Commandes;
    }

    public function confirm(Request $request)
    {
        $article = Article::where('id',$request->id)->first();
        $article->methode = $request->methode;
        $article->save();
        return $request->methode;
    }
}
