<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Previs;
use Illuminate\Http\Request;

class PreviController extends Controller
{

    public function getAllPrevisions(Request $request)
    {
        $previsions = Article::when($request->famille_id, function($q) use ($request){    //Search by Nom Article
                                        $q->where('famille_id',$request->famille_id);})
                    ->with(['previsions' => function($q) use ($request){
                        $q->whereBetween('date', [$request->datePrevd, $request->datePrevf]);
                        }])
                    ->with('famille')
                    ->get();
        return $previsions;
    }
   
    public function getArticlesList()
    {
        $articles = Article::All();
        $data = array();
        $dataf = array();
        $a = array();
        $b = array();
        for ($i = 0; $i < Count($articles); $i++) {
            $data[$articles[$i]->id] = $articles[$i]->code;
            array_push($a, $articles[$i]->famille_id);
            array_push($b, $articles[$i]->id);
        }
        // echo array_search("4146",$data);
        return [
            'articles' => $data,
            'a' => $a,
            'b' => $b
        ];
    }


   public function jsono()
    {
        //Data
        $file = public_path('pre.json');
        $previsions = json_decode(file_get_contents($file),true);
        $previsionsList = array();

        //Articles Data
        $art = $this->getArticlesList();
        $articleList = $art['articles'];
        $familleList = $art['a'];
        $articleidList = $art['b'];


        for($i = 0;$i<count($previsions);$i++){
            $artCode = $previsions[$i]['ARTCODE'];
            $articleID = array_search($previsions[$i]['ARTCODE'],$articleList);
            if($articleID){
                $articleIndex = array_search($articleID, $art['b']);
                $familleID = $familleList[$articleIndex];
                $Dates =  array_slice(array_keys($previsions[$i]),2);
                $vals =  array_slice(array_values($previsions[$i]),2);
                for($k = 0;$k<count($vals);$k++){
                    $prev = new Previs;
                    $prev->article_id = $articleID;
                    $prev->famille_id = $familleID;
                    $prev->code = $artCode;
                    $prev->date = date("Y-m-d", strtotime($Dates[$k]));
                    $prev->prevision = $vals[$k];
                    $prev->save();
                }
            }
           
        }
       return "done";
    }

}
