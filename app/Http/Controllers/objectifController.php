<?php

namespace App\Http\Controllers;

use App\Models\Objectif;
use App\Models\Commande;
use App\Models\Famille;
use App\Models\ArticleObject;
use App\Models\Article;
use App\Models\Stock;
use App\Models\Prix;
use Illuminate\Http\Request;
use DB;

class ObjectifController extends Controller
{

    public function getObjectifRange(Request $request)
    {
        $objectifs = Objectif::selectRaw(DB::raw('id,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
                     ->selectRaw(DB::raw('sum(objectif) as objectifSum'))
                     ->whereBetween('date',[$request->dateDebut,$request->dateFin])
                     ->groupByRaw('extract(MONTH from date)')
                     ->with(['commandes' => function ($q) use ($request) {
                        $q->selectRaw(DB::raw('famille_id,extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year'))
                        ->selectRaw(DB::raw('sum(price) as Sumventes'))
                        ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                        ->groupByRaw('extract(MONTH from date_vente)');
                    }])
                    ->get();
        $cmds = Commande::selectRaw(DB::raw('extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year'))
                    ->selectRaw(DB::raw('sum(price) as Sumventes'))
                    ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                    ->groupByRaw('extract(MONTH from date_vente)')
                    ->get();
        return [
            'objectifs'=>$objectifs,
            'commandes'=>$cmds
        ];
    }

    public function getObjectif(Request $request)
    {
       $familles = Famille::all();
       $objectifs = Objectif::where('date',$request->dateObjectif)
                ->with('famille')
                ->get();
       $cmds = Commande::join('familles','commandes.famille_id','=','familles.id')
                ->selectRaw(DB::raw('famille_id,familles.famille,extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year'))
                ->selectRaw(DB::raw('sum(price) as Sumventes'))
                ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                ->groupByRaw('famille_id,extract(MONTH from date_vente)')
                ->get();
       $mix = Commande::join('familles','commandes.famille_id','=','familles.id')
                ->selectRaw(DB::raw('famille_id,familles.famille'))
                ->selectRaw(DB::raw('sum(price) as Sumventes'))
                ->whereBetween('date_vente', [$request->dateDebutMix, $request->dateFinMix])
                ->groupByRaw('famille_id')
                ->get();
        $mixTotal =  Commande::selectRaw(DB::raw('sum(price) as Sumventes'))
                ->whereBetween('date_vente', [$request->dateDebutMix, $request->dateFinMix])
                ->get();
        $articles = Commande::join('articles','commandes.article_id','=','articles.id')
                ->selectRaw(DB::raw('article_id,articles.code'))
                ->selectRaw(DB::raw('sum(price) as Sumventes,sum(quantite) as qte,(sum(price)/sum(quantite)) as PrixMoy'))
                ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                ->groupByRaw('article_id')
                ->get();

        return [
        'objectifs'=>$objectifs,
        'commandes'=>$cmds,
        'familles'=>$familles,
        'articles'=>$articles,
        'mix'=>$mix,
        'mixTotal'=>$mixTotal
        ];
    }

   
    public function editObjectifs(Request $request)
    {
       $data =  $this->getObjectif($request);
       $mix = $data['mix'];
       $mixTotal = $data['mixTotal'];

       for($i = 0;$i<count($mix);$i++){

            $objectif = $request->objectif*((float)$mix[$i]['Sumventes'])/((float)$mixTotal[0]['Sumventes']);

            $obj = Objectif::where('famille_id',$mix[$i]['famille_id'])->where('date',$request->dateObjectif)->first();
            if($obj){
               $obj->objectif = round($objectif,2);
               $obj->save();
            }else{
                $add = new Objectif;
                $add->famille_id = $mix[$i]['famille_id'];
                $add->date = $request->dateObjectif;
                $add->objectif = round($objectif,2);
                $add->save();
            }
       }
       return 1;
    }
   
    public function getFamilleObjectif(Request $request)
    {
        $famille = Famille::where('famille',$request->famille)->first();
        $obj = Objectif::where('famille_id',$famille->id)->where('date',$request->date)->first();
        return $obj;
        
    }
   
    public function EditFamilleObjectif(Request $request)
    {
        $obj = Objectif::where('id',$request->id)->first();
        $obj->objectif = $request->objectif;
        $obj->cause = $request->cause;
        $obj->save();
        return $obj;
        
    }

    public function EditArticleObjectif(Request $request)
    {
        $obj = ArticleObject::where('article_id',$request->article_id)->where('date',$request->dateObjectif)->first();
        if($obj){
            $obj->objectif = $request->newObjectif;
            $obj->save();
            $famObj = Objectif::where('famille_id',$request->famille_id)->where('date',$request->dateObjectif)->first();
            if($famObj){
                $obje = $famObj->objectif;
                $cal = $obje-$request->lastObjectif+$request->newObjectif;
                $famObj->objectif = $cal;
                $famObj->save();
            }
        }else{
            $newObj = new ArticleObject;
            $newObj->article_id = $request->article_id;
            $newObj->famille_id = $request->famille_id;
            $newObj->date = $request->dateObjectif;
            $newObj->objectif = $request->newObjectif;
            $newObj->save();
            $famObj = Objectif::where('famille_id',$request->famille_id)->where('date',$request->dateObjectif)->first();
            if($famObj){
                $obje = $famObj->objectif;
                $cal = $obje-$request->lastObjectif+$request->newObjectif;
                $famObj->objectif = $cal;
                $famObj->save();
            }
        }
        return 1;
    }
    

    public function getArticlesObjectifs(Request $request)
    {
        // $objectifs = ArticleObject::where('famille_id',$request->familleid)
        //             ->with('article')
        //             ->get();
        // return $objectifs;

        $Familleobjectif = Objectif::where('date',$request->dateObjectif)
        ->where('famille_id',$request->famille_id)
        ->get();
        $cmds = Commande::join('articles','commandes.article_id','=','articles.id')
                ->selectRaw(DB::raw('article_id,articles.code,articles.designation,extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year'))
                ->selectRaw(DB::raw('sum(price) as Sumventes,sum(quantite) as qte'))
                ->where('commandes.famille_id',$request->famille_id)
                ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                ->groupByRaw('article_id,extract(MONTH from date_vente)')
                ->get();
        $mix = Commande::join('articles','commandes.article_id','=','articles.id')
                ->selectRaw(DB::raw('article_id,articles.code,articles.designation'))
                ->selectRaw(DB::raw('sum(price) as Sumventes'))
                ->where('commandes.famille_id',$request->famille_id)
                ->whereBetween('date_vente', [$request->dateDebutMix, $request->dateFinMix])
                
                ->groupByRaw('article_id')
                ->get();
        $mixTotal =  Commande::selectRaw(DB::raw('sum(price) as Sumventes'))
                ->whereBetween('date_vente', [$request->dateDebutMix, $request->dateFinMix])
                ->where('famille_id',$request->famille_id)
                ->get();
        $objectifs = ArticleObject::where('famille_id',$request->famille_id)
                ->where('date',$request->dateObjectif)
                ->get();
        $articles = Article::where('famille_id',$request->famille_id)
                ->get();
        $stock = Stock::selectRaw(DB::raw('famille_id,article_id,sum(quantite) as stock'))
                ->where('famille_id',$request->famille_id)
                ->whereIn('date',function($query) use($request){
                    $query->selectRaw(DB::raw('max(date)'))
                    ->whereBetween('date',[$request->dateDebut,$request->dateFin])
                    ->groupByRaw('extract(month from DATE)');
                })->get();

        return [
        'objectifFamille'=>$Familleobjectif,
        'commandes'=>$cmds,
        'articles'=>$articles,
        'mix'=>$mix,
        'mixTotal'=>$mixTotal,
        'objectifs'=>$objectifs,
        'stocks'=>$stock
        ];
    }

}
