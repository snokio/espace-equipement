<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Famille;
use App\Models\Zone;
use App\Models\Prevision;
use App\Models\Taux;
use App\Models\Article;
use App\Models\Alpha;
use App\Models\Confirmation;
use DB;

use Illuminate\Http\Request;

class GenerationController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // public function getPrevision(Request $request)
    // {
    //     $previsions = Article::with(['previsions' => function ($q) {
    //         $q->selectRaw(DB::raw('article_id,prevision,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'));
    //     }])
    //         ->with(['stocks' => function ($q) use ($request) {
    //             $q->selectRaw(DB::raw('article_id,quantite,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
    //                 ->whereBetween('date', [$request->dateDebut, $request->dateFin]);
    //         }])
    //         ->get();
    //     return $previsions;
    // }

    public function getArticlebyname($article)
    {
        $article = Article::where('designation',$article)->first();
        return $article;
    }

    public function getArticleData(Request $request)
    {
        $data = Article::when($request->code, function($q) use ($request){    //Search by Nom Article
                            $q->where('code',$request->code);
                        })
                        ->when($request->article, function($q) use ($request){    //Search by Nom Article
                            $q->where('designation', 'like', '%'.$request->article.'%');
                        })
                        ->when($request->famille, function($query) use ($request){                   //Search by Famille
                            $query->whereHas('famille', function($q) use ($request) {
                                $q->where('famille',$request->famille);
                            });
                        })
                        ->with('tau') 
                        ->with(['previsions' => function ($q) use($request) {
                            $q->selectRaw(DB::raw('article_id,prevision,extract(WEEK from date) as Week,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
                             ->whereBetween('date', [$request->datePrevDebut, $request->datePrevFin])
                             ->groupByRaw('article_id,extract(WEEK FROM date),extract(MONTH from date),extract(YEAR FROM date)')
                             ->orderBy('Year','asc')
                             ->orderBy('Mois','asc')
                             ->orderBy('Week','asc');
                         }])
                        ->with(['stocks' => function ($q) use ($request) {
                             $q->selectRaw(DB::raw('article_id,quantite,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year,Extract(WEEK FROM date) as Week'))
                             ->whereBetween('date', [$request->dateStockDebut, $request->dateStockFin])
                             ->groupByRaw('article_id,extract(MONTH from date),extract(WEEK FROM date)')
                             ->orderBy('Year','asc')
                             ->orderBy('Mois','asc')
                             ->orderBy('Week','asc');
                            }])
                        ->with(['stockone' => function ($q) use ($request) {
                             $q->selectRaw(DB::raw('id,article_id,quantite,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
                             ->whereBetween('date', [$request->dateStockDebut, $request->dateStockFin])
                             ->groupByRaw('id,extract(MONTH from date),extract(WEEK FROM date)')
                             ->orderBy('id','desc')
                             ->orderBy('Year','asc')
                             ->orderBy('Mois','asc');
                            }])
                        ->with(['commandes' => function ($q) use ($request) {
                             $q->selectRaw(DB::raw('article_id,quantite,extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year,Extract(WEEK FROM date_vente) as Week'))
                             ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
                             ->whereBetween('date_vente', [$request->dateCmdDebut, $request->dateCmdFin])
                             ->groupByRaw('article_id,extract(MONTH from date_vente),extract(WEEK FROM date_vente)')
                             ->orderBy('Year','asc')
                             ->orderBy('Mois','asc')
                             ->orderBy('Week','asc');
                            }])
                        ->with(['previsionsS' => function ($q) use($request) {
                                $q->selectRaw(DB::raw('article_id,prevision,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
                                ->selectRaw(DB::raw('sum(prevision) as Sumprev'))
                                 ->whereBetween('date', [$request->datePrevDebut, $request->datePrevFin])
                                 ->groupByRaw('article_id,extract(MONTH from date),extract(YEAR FROM date)')
                                 ->orderBy('Year','asc')
                                 ->orderBy('Mois','asc');
                             }])
                        ->with(['previsionsT' => function ($q) use($request) {
                                $q->selectRaw(DB::raw('article_id,prevision,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
                                ->selectRaw(DB::raw('sum(prevision) as Sumprev'))
                                 ->whereBetween('date', [$request->datetd, $request->datetf])
                                 ->groupByRaw('article_id,extract(MONTH from date),extract(YEAR FROM date)')
                                 ->orderBy('Year','asc')
                                 ->orderBy('Mois','asc');
                             }])
                        ->with(['stocksM' => function ($q) use ($request) {
                            $q->selectRaw(DB::raw('id,date,article_id,quantite,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year,Extract(WEEK FROM date) as Week'))
                            ->whereBetween('date', [$request->dateStockDebut, $request->dateStockFin])
                            ->orderBy('date','desc')
                            ->orderBy('id','desc')
                            ->orderBy('Year','desc')
                            ->orderBy('Mois','desc')
                            ->orderBy('Week','desc');
                           }])
                        ->with(['commandesM' => function ($q) use ($request) {
                             $q->selectRaw(DB::raw('article_id,extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year'))
                             ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
                             ->whereBetween('date_vente', [$request->dateCmdDebut, $request->dateCmdFin])
                             ->groupByRaw('article_id,extract(MONTH from date_vente)')
                             ->orderBy('Year','asc')
                             ->orderBy('Mois','asc');
                            }])
                        ->with(['commandesX' => function ($q) use ($request) {
                             $q->selectRaw(DB::raw('article_id,extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year'))
                             ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
                             ->whereBetween('date_vente', [$request->datetd, $request->datetf])
                             ->groupByRaw('article_id,extract(MONTH from date_vente)')
                             ->orderBy('Year','asc')
                             ->orderBy('Mois','asc');
                            }])
                        ->with(['cmdplanifie' => function ($q) use($request) {
                                $q->selectRaw(DB::raw('article_id,sum(quantite) as cmds,extract(MONTH from date_plan) AS Mois,Extract(YEAR FROM date_plan) as Year'))
                                 ->whereBetween('date_plan', [$request->datePrevDebut, $request->datePrevFin])
                                 ->groupByRaw('article_id,extract(MONTH from date_plan),extract(YEAR FROM date_plan)')
                                 ->orderBy('Year','asc')
                                 ->orderBy('Mois','asc');
                             }])
                        ->with(['achatplanifie' => function ($q) use($request) {
                                $q->selectRaw(DB::raw('article_id,confID,sum(quantite) as cmds,extract(MONTH from date_reception) AS Mois,Extract(YEAR FROM date_reception) as Year'))
                                 ->whereBetween('date_reception', [$request->datePrevDebut, $request->datePrevFin])
                                 ->groupByRaw('article_id,extract(MONTH from date_reception),extract(YEAR FROM date_reception)')
                                 ->orderBy('Year','asc')
                                 ->orderBy('Mois','asc');
                             }])
                        ->with(['achatplanifieConf' => function ($q) use($request) {
                                $q->selectRaw(DB::raw('article_id,confID,quantite,extract(MONTH from date_reception) AS Mois,Extract(YEAR FROM date_reception) as Year,extract(MONTH from date_achat) AS MoisA,Extract(YEAR FROM date_achat) as YearA'))
                                ->whereNotNull('confID') 
                                ->whereBetween('date_reception', [$request->datePrevDebut, $request->datePrevFin])
                                 ->orderBy('Year','asc')
                                 ->orderBy('Mois','asc');
                             }])
                        ->with('confirmations')
                        ->with('parametres')
                        ->whereHas('previsionsS', function ($query) use($request){
                            $query->whereBetween('date', [$request->datePrevDebut, $request->datePrevFin]);
                        })
                        ->paginate(10);
        return $data;
    }

    public function confirmData(Request $request)
    {
        $article = Article::Where('code',$request->article_code)->first();
        $confirmationlst = Confirmation::latest('id')->first();
        $checkConfirmation = Confirmation::where('article_id',$article->id)->where('month',$request->month)->first();
        if($checkConfirmation){
            $confirmation = $checkConfirmation;
            $confirmation->po_release = $request->po_release;
            $confirmation->month_rec = $request->month_rec;
            $confirmation->date = $request->date;
            $confirmation->save();
            return $confirmation;
        }else{
            $confirmation = new Confirmation;
            $confirmation->article_id = $article->id;
            $confirmation->famille_id = $article->famille_id;
            $confirmation->article_code = $request->article_code;
            $confirmation->month = $request->month;
            $confirmation->month_rec = $request->month_rec;
            $confirmation->date = $request->date;
            $confirmation->po_release = $request->po_release;
            $confirmation->confirmationID = 'cof-'.($confirmationlst->id+1);
            $confirmation->save();
            return $confirmation;
        }
       
    }

    public function getPo(Request $request)
    {
        $confirmations = Confirmation::whereBetween('date',[$request->first,$request->last])->get();
        return $confirmations;
    }


    public function getPublishedConfirmation()
    {
        $dateD = date('Y-m-01');
        $dateF = date('Y-m-t',strtotime($dateD));
        $conf = Confirmation::join('familles','confirmation.famille_id','=','familles.id')
        ->join('articles','confirmation.article_id','=','articles.id')
        ->whereBetween('date',[$dateD,$dateF])
        ->get();
        return $conf;
    }

}
