<?php

namespace App\Http\Controllers;

use App\Models\Achat;
use App\Models\Achatplan;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Famille;
use App\Models\Commande;
use App\Models\Article;
use App\Models\Zone;
use App\Models\Client;
use App\Models\Taux;
use App\Models\Alpha;
use App\Models\CmdPlan;
use DB;
use DateTime;
use App\Models\Log;
use App\Models\Prevision;
use Illuminate\Http\Request;

class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function prev()
    {
        $articles = array('170F','178F/25','178F/FA','178FE/G','186F/FA','186FE/G','186FS','3WF-3A','4GS11M-4OS','4GS15M-4OS','4MF14/8','4MFF95/20','4SD-4/14','4SD4/18','4SDM-2/24','4SDM-2/35+','4SDM4/18','4SWP10-10','4SWP10-14','4SWP2-19','4SWP2-25','4SWP4-13','4SWP4-17','4TF14/30','4TF24/34','52CC-A','5SM-205','5SP35-140','5SP35-140F','A7062-XJ','AAC1408','ABN15501','AC20248','AC25508','ACE815','ACE914','ACS175246','ACUATEC40','ACUATEC60','ACUATEC70','ACUATEC80','AKT0051','ALT-3M','ALT-5M','ALT-6M','ALT-ROT3M','ALT-ROT5M','ALT-ROT5T','ALT-ROT6M','ALT-ROT6T','ALT-STAT3M','ALT-STAT5M','ALT-STAT5T','ALT-STAT6M','ALT-STAT6T','AMRT0044','AMRT0045','AMRT0046','AMRT0061','AMRT0062','AMRT0100','AMRT0116','ANTI-ABR12/21','AP2055AL','AP2065AL','ARO19/50-IT','ART0013','ART0015','ART0016','ART0017','ART0018','ART0019','ART0020','ART0021','ART0022','ART0023','ART0025','ART0026','ART0027','ART0028','ART0029','ART0030','ART0031','ART0032','ART0033','ART0034','ART0035','ART0036','ART0037','ART0038','ART0039','ART0046','ART0047','ART0049','ART0054','ART0056','ART0057','ART0058','ART0060','ART0061','ART0062','ART0064','ART0066','ART0069','ART0070','ART0072','ART0073','ART0074','ART0075','ART0076','ART0077','ART0078','ART0079','ART0080','ART0081','ART0082-C1','ART0083-C1','ART0084-C1','ART0085-C1','ART0086-C1','ART0086-C2','ART0088','ART0089','ART0090','ART0091','ART0092','ART0093','ART0094','ART0095','ART0096','ART0097','ART0098','ART0099-C1','ART0099-C2','ART0100-C1','ART0100-C2','ART0101-C1','ART0101-C2','ART0102-C1','ART0102-C2','ART0104','ART0105','ART0106','ART0107','ART0110','ART0111','ART0112','ART0113','ART0114','ART0115','ART0116','ART0117','ART0118','ART0119','ART0120','ART0121','ART0123','ART0124','ART0125','ART0126','ART0127','ART0128','ART0130','ART0131','ART0133','ART0137','ART0138','ART0139','ART0140','ART0141','ART0142','ART0143','ART0144','ART0145','ART0146','ART0147-C1','ART0148-C1','ART0149-C1','ART0149-C2','ART0150-C1','ART0150-C2','ART0151-C1','ART0151-C2','ART0152','ART0153','ART0154','ART0155','ART0156','ART0157','ART0158','ART0160','ART0163','ART0164','ART0165','ART0166','ART0168','ART0172','ART0173','ART0174','ART0175','ART0176','ART0177','ART0178','ART0179','ART0180','ART0181','ART0182','ART0183','ART0185','ART0187','ART0188','ART0189','ART0190','ART0191','ART0192','ART0193','ART0194','ART0195','ART0196','ART0197','ART0198','ART0199','ART0200','ART0201','ART0202','ART0203','ART0204','ART0205','ART0206','ART0207','ART0208','ART0209','ART0210','ART0211','ART0212','ART0214','ART0217','ART0218','ART0219','ART0220','ART0221','ART0224','ART0225','ART0226','ART0227','ART0228','ART0229','ART0230','ART0231','ART0232','ART0233','ART0234','ART0235','ART0237','ART0240','ART0243','ART0244','ART0245','ART0247','ART0248','ART0249','ART0250','ART0251','ART0254','ART0255','ART0256','ART0257','ART0263','ART0277','ART0278','ART0283','ART0285','ART0286','ART0287','ART0288','ART0289','ART0290','ART0291','ART0292','ART0293','ART0294','ART0295','ART0298','ART0299','ART0300','ART0301','ART0302','ART0303','ART0304','ART0305','ART0306','ART0309','ART0310','ART0311','ART0312','ART0313','ART0314','ART0315','ART0325','ART0326','ART0327','ART0328','ART0329','ART0330','ART0331','ART0335','ART0336','ART0337','ART0338','ART0339','ART0340','ART0341','ART0342','ART0343','ART0344','ART0345','ART0346','ART0347','ART0348','ART0349','ART0350','ART0351','ART0352','ART0353','ART0355','ART0356','ART0367','ART0368','ART0369','ART0370','ART0371','ART0372','ART0386','ART0387','ART0388','ART0389-C1','ART0389-C2','ART0390','ART0391','ART0392-C1','ART0392-C2','ART0393-C1','ART0393-C2','ART0394','ART0395-C1','ART0395-C2','ART0396-C1','ART0396-C2','ART0399','ART0400','ART0401','ART0402','ART0403','ART0404','ART0405','ART0406','ART0407','ART0408','ART0409','ART0410','ART0411','ART0412','ART0413','ART0414','ART0415','ART0416','ART0420','ART0421','ART0422','ART0438','ART0439','ART0440-C1','ART0440-C2','ART0441-C1','ART0441-C2','ART0442-C1','ART0442-C2','ART0443-C1','ART0444-C1','ART0446','ART0447','ART0448','ART0449','ART0450','ART0451','ART0452','ART0453','ART0454','ART0455','ART0458','ART0460','ART0461','ART0462','ART0463','ART0464','ART0465','ART0466','ART0467','ART0468-C1','ART0468-C2','ART0469','ART0470','ART0471','ART0472','ART0473','ART0474','ART0475','ART0476','ART0477','ART0478','ART0479','ART0480','ART0481','ART0483','ART0484','ART0485','ART0486','ART0487','ART0488','ART0489','ART0490','ART0491','ART0492','ART0493','ART0494','ART0495','ART0496','ART0497','ART0498','ART0499','ART0500','ART0501','ART0502','ART0503','ART0504','ART0505','ART0506','ART0507','ART0508','ART0509','ART0510','ART0511','ART0512','ART0513','ART0514','ART0515','ART0516','ART0517','ART0518','ART0519','ART0520','ART0521','ART0522','ART0523','ART0524','ART0525','ART0526','ART0527','ART0593','ART0594','ART0597','ART0598','ART0599','ART0600','ART0601','ART0602','ART0603','ART0605','ART0606','ART0612','ART0613','ART0614','ART0615','ART0616','ART0617','ART0618','ART0619','ART0620','ART0621','ART0625','ART0626','ART0630','ART0631','ART0635','ART0639','ART0643','ART0644','ART0645','ART0647','ART0648','ART0649','ART0650','ART0651','ART0652','ART0653','ART0654','ART0655','ART0656','ART0657','ART0658','ART0659','ART0660','ART0661','ART0662','ART0663','ART0664','ART0665','ART0666','ART0667','ART0668','ART0669','ART0670','ART0671','ART0672','ART0673','ART0674','ART0675','ART0676','ART0677','ART0678','ART0679','ART0680','ART0681','ART0682','ART0683','ART0684','ART0685','ART0686','ART0687','ART0690','ART0691','ART0692','ART0693','ART0694','ART0695','ART0696','ART0697','ART0698','ART0699','ART0700','ART0701','ART0702','ART0703','ART0704','ART0705','ART0706','ART0707','ART0708','ART0709','ART0710','ART0711','ART0712','ART0713','ART0714','ART0715','ART0716','ART0717','ART0718','ART0719','ART0720','ART0721','ART0722','ART0723','ART0724','ART0725','ART0726','ART0727','ART0728','ART0729','ART0730','ART0731','ART0732','ART0733','ART0734','ART0735','ART0736','ART0737','ART0738','ART0739','ART0740','ART0741','ART0742','ART0743','ART0744','ART0745','ART0746','ART0747','ART0748','ART0749','ART0750','ART0751','ART0752','ART0753','ART0754','ART0755','ART0756','ART0757','ART0758','ART0759','ART0760','ART0761','ART0762','ART0763','ART0764','ART0765','ART0766','ART0767','ART0768','ART0769','ART0770','ART0771','ART0772','ART0773','ART0774','ART0775','ART0776','ART0777','ART0778','ART0779','ART0780','ART0781','ART0782','ART0783','ART0784','ART0785','ART0786','ART0787','ART0788','ART0789','ART0790','ART0791','ART0792','ART0793','ART0794','ART0795','ART0796','ART0797','ART0798','ART0799','ART0800','ART0801','ART0802','ART0803','ART0804','ART0826','ART0827','ASP-CAN1P-A','ASP-CAN1P-B','ASP-CAN-PLAS1.5','ASPM-8035','ASP-R2/L','ASP-R2/M','ASP-R3/L','ASP-R3/M','ASP-RC130/PL','ATG0601','ATLD-2000MPAS','ATLE-2000MPAS','ATOM-16L','ATOM-16LIN','ATOM-20L','ATOM-20PLAST','ATOM-767K','AVR-ALT-5T','AVR-ALT-6T','AVRM-2KW','AVRM-5KW','AVRT-5KW','AXE','B350HD','BA-186FS','BAM-EY20','BAM-GX160/168','BAR-GF','BAR-PF','BATT-MDI','BD-13','BD-5.5','BD-DEB430','BD-EY','BD-FL950','BET750EH+','BETONNIERE-350L','BIEL-13','BIEL-178F','BIEL-EY','BIEL-GPX160','BIEL-GPX168','BMS-6','BOBINE-FUS','BOUCH-75/IT','BOU-MP','BRID-ME5.5','BRUM-4B','BUSE1010','BUT-AR/MA','BUT-AR/MA/23','BX1-250C/BL','BX1-315/BL','BX1-400/BL','BX1-500/BL','BX1-630/BL','BX6-250/BL','BX6-300/BL','BX6-400/BL','C160-EY28','C90-RB20','CACH-ALT5M','CAIS-53BM','CA-PP-22','CA-PP-30','CARB-13','CARB-6.5','CARB-DEB430','CARB-EY','CARB-FL950','CARB-JF390','CART-GPX168','CART-PULV-RB20F','CBA1.5M-DEV','CBA10T-DEV','CBM1.5M-ATL','CBM2M-DEV','CBM4T-DEV','CHAND-3','CHAND-6','CHAR-ALT-5T','CHAR-ALT-6T','CH-BAT/R-20','CH-BAT/R-30','CH-BAT/R-50','CHEM-ZS1110','CHEM-ZS1115','CI0.75','CI2.5-AQUA','CI4','CIDLI228180','CIWLI2001','CLAP-1','CLAP-1.5','CLAP-PP-22','CLAP-PP-30','CLL-N','CLT-1M','CM-004','CM-005','CM-006','CM-007.5','CM-008','CM-010','CM-012','CM-014','CM-016','CM-016-C','CM-018','CM-020','CM-025','CM-025-C','CM-030','CM-030-C','CM-035','CM-035-C','CM-040','CM-040-C','CM-045','CM-045-C','CM-050','CM-050-C','CM-055','CM-060','CM-060-C','CM-070','CM-070-C','CM-080','CM-100','CM-100-C','COL-PR63-1.25IT','COL-PR63-1.5IT','COL-PR75-0.75IT','COL-PR75-1.25IT','COL-PRI32-0.5IT','COL-PRI32-0.75I','COL-PRI40-0.5IT','COL-PRI40-1P-IT','COL-PRI50-0.5IT','COL-PRI50-0.75I','COL-PRI50-1P-IT','COL-PRI63-0.5IT','COL-PRI63-0.75I','COL-PRI63-1P-IT','COL-PRI75-1.5IT','COL-PRI75-1P-IT','COL-SER104-112I','COL-SER104-115I','COL-SER110-123I','COL-SER113-121I','COL-SER122-130I','COL-SER26-28INO','COL-SER29-31INO','COL-SER3.6X200','COL-SER30-33INO','COL-SER32-35INO','COL-SER34-37INO','COL-SER36-39INO','COL-SER38-41INO','COL-SER40-43INO','COL-SER42-46INO','COL-SER47-51INO','COL-SER52-55INO','COL-SER56-59INO','COL-SER58-63INO','COL-SER60-63INO','COL-SER64-67INO','COL-SER67-73INO','COL-SER7.6X380','COL-SER7.6X500','COL-SER74-79INO','COL-SER80-85INO','COL-SER86-91INO','COL-SER95-105IN','COL-SER98-103IN','CO-MC','COMP200L','COND-0.65KW','COND-075-D','COND-100-D','COND-150-D','COND-200-D','COND-300-D','COND-350-D','COND-400-D','COND-500-D','COND-800-D','COND-ALT3M','COND-ALT5M','COND-ALT6M','COS35528','COUD90-110','COUD90-40','COUD90-75','COUD90-90','COUD-EGA32','COUD-EGA32/IT','COUD-EGA40','COUD-EGA50','COUD-EGA50/IT','COUD-EGA63/IT','COUD-EGA75','COUD-F32','COUD-F32/IT','COUD-F40','COUD-M32','COUD-M40','COUD-M40/IT','COUD-M63/IT','COUP-CT-50','COUP-CT-50-M','COUR-PP-22','COUS-178-C','COUS-178F','COUS-186-C','COUS-186F','COUV-6500CXE','CPL-PIM','CPL-VIB','CPM130','CPM158','CPM158-B','CPM200','CPM300','CPM300A','CPT200','CPT200-A','CPT300','CPT300-A','CRB10','CRB2','CRB20','CRB4','CRB5','CRB6','CREP-1.25P','CREP-1.25P-RES','CREP-1.5P-RES','CREP-1P-RES','CREP-2.5P','CREP-2.5P-RES','CREP-2P','CREP-2P-RES','CRG200','CRG225','CRG300','CRG300B','CRG300-L/OR','CRG300-L2','CULB-178F','CULB-186F','DCM6002','DEB-LAM402T','DEB-LAME3T','DEBR430-6-C1','DEBR430-6-C2','DEB-TET1','DIOD-ALT5M','DISC-SCIE14','DISC-SCIE16','DMP-240','DMP-250','DMP-270','EMBR-TAL','EY20-3C','EY28','FC-10/1000','FC-10/1000G','FC-10/250','FC-10/500','FC10-G','FC-12/1000','FC-12/220','FC-12/220G','FC-12/500','FC-22','FC-6/1000','FC-6/250','FC-6/250G','FC-6G/1000','FC-8/1000','FC-8/1000G','FC-8/250','FC-8/250G','FC-8/500','FC-8/500G','FILTA-178F','FILTA-186F','FILTA-JF390','FILTA-ZS1110','FILT-COMPRES','FILTD-178F','FILTD-186F','FILTD-R180N','FILTH-ZS1110','FILTH-ZS1115','FILTSUGCANE','FL22A-1','FL30A-1','FL3700DX','FL950','FOR10/210+','FOR10/260+','FOR10/260M','FOR10/360+','FOR12/260+','FOR12/260M','FOR12/360+','FOR14/260+','FOR14/260M','FOR14/360+','FOR16/260+','FOR16/260M','FOR16/350+','FOR16/400M','FOR16/450+','FOR16/505M','FOR18/260M','FOR18/300+','FOR18/350+','FOR18/400M','FOR18/450+','FOR18/500M','FOR20/260M','FOR20/350+','FOR20/450+','FOR20/505M','FOR20/600+','FOR6/160+','FOR8/160+','FOR8/210+','FOR8/260+','FRG-178M','FRG-186FM','FRG-186FT','FRM-GPX168','FRP-E','FRV','FS8A','FS8A-10M','FS8A-3M','FS8A-5M','FUS-GF','GB18-15','GE30005','GO2-350','GP400','GPX168','GRT2-C1','GRT2-C2','HBS1BLANC','HBS1NOIR','HBS1ORANGE','HBS2FLAMME','HBS3NOIR','HCC18-188','HCSPA101','HG20008','HKTHP10811','HKTHP11021','HKTHP11111','HPWR20008','HPWR28008','HZN2R-38','HZN2R-45','HZN38-E','IF-M400-72','ING-MMAC2002','INJECT-178F','INJECT-186F','INJECTCO-178F','INJECTCO-186F','INJECTCO-A-186F','INJECTCO-ZS1110','INJECTCO-ZS1115','INJECT-ZS1110','IW10508','JAU-FL950','JC-178F','JC-186F','JC-EY','JC-GPX168','JC-JF390','JC-ZS1110','JC-ZS1115','JF390','JH-PP','JOIN-TO/MP2','JOIN-TO/MP3','JP-GPX160','JP-GPX168','JS40028','JS-GPX160','JS-JF390','KIP-178FAGE','KIP-186FAGE','KIP-188FAGE','KIP-6500E3','KIT-CHA-1','KIT-M11','KIT-SRP','KIT-SRP-TI','LAA-N','LAME-SM1000','LAME-SM1200','L-ART0006','L-ART0007','L-ART0009','L-ART0010','L-ART0011','L-ART0017','L-ART0019','L-ART0020','L-ART0021','L-ART0022','L-ART0025','L-ART0026','L-ART0027','L-ART0040','L-ART0044','L-ART0047','L-ART0048','L-ART0052','L-ART0053','L-ART0054','L-ART0060','L-ART0061','L-ART0066','L-ART0068','L-ART0069','L-ART0070','L-ART0071','L-ART0072','L-ART0073','L-ART0074','L-ART0075','L-ART0076','L-ART0077','L-ART0078','L-ART0079','L-ART0080','L-ART0083','L-ART0084','L-ART0085','L-ART0086','L-ART0092','L-ART0098','L-ART0099','L-ART0101','L-ART0106','L-ART0107','L-ART0108','L-ART0109','L-ART0110','L-ART0111','L-ART0112','L-ART0114','L-ART0115','L-ART0116','L-ART0117','L-ART0118','L-ART0121','L-ART0122','L-ART0123','L-ART0124','L-ART0126','L-ART0127','L-ART0128','L-ART0129','L-ART0130','L-ART0131','L-ART0132','L-ART0133','L-ART0134','L-ART0135','L-ART0136','L-ART0137','L-ART0138','L-ART0139','L-ART0140','L-ART0141','L-ART0143','L-ART0144','L-ART0145','L-ART0146','L-ART0147','L-ART0148','L-ART0149','L-ART0150','L-ART0151','L-ART0152','L-ART0153','L-ART0154','L-ART0155','L-ART0156','L-ART0157','L-ART0159','L-ART0160','L-ART0162','L-ART0163','L-ART0168','L-ART0171','L-ART0172','L-ART0173','L-ART0174','L-CDC/AC24V1000W','L-CPDC/AC24V1000W','L-KIT-AVEL','LLA-N','L-PF2P40','L-PF3P40','L-PRESS-PM5-ITALY','L-SRP-HT24TR','LTP','MAG-115','MAM-1.5P','MAM-1P','MANCH-25','MANCH-40','MANCH50-40RE/IT','MANCH-63/IT','MANCH63-32RE/IT','MANCH63-50RE/IT','MANCH-75','MANCH-90/IT','MANOM-SRP','MART-ALT/H','MART-ALT/V','MART-ALT/V2','MART-BO/J','MART-HD26','MART-MP','MART-PB','MB300','MB300+','MB550','MB550+','MB6GM-140A','MB6GM-140NN','MB6GM-150N','MBCFG-20','MBHSG-1000','MBHSG-600','MC14008','MC-BA','MC-BSM','MEM0.75-4/YL','MEM1.5-2','MEM1.5-4/YC','MEM1-4/YC','MEM2-2','MEM2-4','MEM3-2','MEM3-4','MEM4-2/YL','MEM5.5-2','MEM5.5-4','MEM5.5-4/B','MET10-4','MET15-4+','MET3-2','MET3-4','MET4-2','MET4-4','MET5.5-2','MET5.5-4','MET7.5-4','MEUL-230','MG1308','MH-08-22','MH-08-30','MH-09-22','MH-09-28','MH-09-30','MIM1.5','MIM1.5H','MIM1.5N','MIM2','MIM2H','MIM2N','MIM3','MIM3H','MIM3N','MIT10','MIT10H','MIT2','MIT2-H','MIT3','MIT3-H','MIT4H','MIT5.5','MIT5.5H','MIT5.5-IT','MIT7.5','MIT7.5H','MIT7.5-IT','MO-10','MO-8','MP2-EY','MP2-GPX168','MP2-GPXNM','MP2-RB','MP3-EY','MP3-GPX168','MP3-GPXNM','MP3-RB','MX214001','MX218008','NHP-180R2','NHP-180R4','NS-96-B/17','PAT-178F','PAT-186-188','PBMIS14002','PC-10','PINJ-178F','PINJ-186F','PINJ-R180N','PINJ-ZS1110','PIS-178F','PIS-186F','PIST-ZS1110','PIST-ZS1115','PKM60','PKM60-B','PKM70','PMP2','PMP2-170F','PMP3','PMP3-178F','PO-CLAP-22','PO-CLAP-30','POUL/EY-MC','POUL-EY','POUL-PP2','POUL-PP3','PR1-10','PR1-250','PR1-270','PR1-610','PR2-10/70','PR2-350','PR2-360','PR2-370','PR2-380','PR3-510','PR3-510+','PR3-520','PR5-210/3/11','PR6-10','PR6-50','PRESS-COMPR','PRESS-COMPR3','PRESS-SRP','PRESS-SRP-TI','PRMP-PE45/21','PRPS-PE103/12','PRPS-PE103/14','PSY55-A','PUL-22A/GPX-2X50','PUL-22A/RB-2X50','PUL-30A/RB-2X50','PULM-22A/178F','PULM-22A/GPX168','PULM-22A/RB20','PUL-POR18','R','R180','R180N','RACC5V-SRP','RAC-F40','RAC-F40/IT','RAC-F90/IT','RAC-M32','RAC-M40','RAC-M63','RAC-M75','RAC-M75/IT','RAC-M90','RAC-M90/IT','RAC-MP2','RAC-MP3','RAC-TUY-PULV/MF','RB20-3C+','RED1.25/1-BR','RED1.5/1-BR','RED2/1.5-BR','RES-168F','RES-186F','RES-6500CXE','RES-6500DXE','RES-FL950','RGH9018','RH120068','RH12008','ROB-FLOT-1.5P','ROB-FLOT-2P','ROB-PP','RO-COMPR','ROT-MART','ROT-MART65B','ROT-MART-PB','RP-PP','RS3208','RS8008','SBSC','SC5200-B','SCM3','SCM3A','SCM4','SCM4A','SCM5A','SCM6','SCM6A','SCM7','SCM7A','SCM8A','SEAU/MC1','SEG-178F','SEG-186F','SEG-ZS1110','SEG-ZS1115','SFM-2M-C1','SFM-3M-GX35-C1','SIL-T90','SOND-N','SOUF-350','SOUP-178F','SOUP-186F','SOUP-GPX160/168','SOUP-JF390','SOUP-RB20','SP35-140','SP35-140F','SPG3508','SPG5008-2','SRP-24PIED.SA','SRP-24VERT.SA','SRP-FL100','SRP-FL24','SRP-FL50','SRP-FL60','SRP-IB500','STA-MART','STA-MART-PB','STC-10KW','STC-12KW','STC-15KW','STC-20KW','STC-25KW','STC-30KW','STC-5KW','STC-7.5KW','STGS7115-B9','STSM1510-B5','STXH2000-B8','SUP-COMPR','SW-BX6','SW-BX6-25','SXM70-18','T25-VC-20','T40-A-30','TAI6010F','TAPSUGCANE','TARI20','TEFLON-GF','TES-EGA25','TES-EGA32','TES-EGA40/IT','TES-EGA50/IT','TES-EGA63/IT','TES-EGA75/IT','TES-EGA90/IT','TES-F40','TES-F40/IT','TES-F50','TES-F50/IT','TES-F63/IT','TES-F75/IT','TES-F90/IT','TIR-178F','TIR-186F','TIR-5.5','TIR-DEB430','TIR-FL950','TIR-JF390','TRI-MB6GM','TRP-MB6GM','TS15008','TUB-186','TUR-MP2','TUR-MP3','TUY/AIR-HP19/50IT','TUY100-PUL','TUY50-PUL','TUY-ARO25/50IT','TUY-ATOM16/20','TUY-HP8/40A','TUY-R100/50','TUY-R50/50','TUY-R63/50','TUY-R75/50','V1100F','V1500F','V2200F','V750F','VAN40-PEHD','VAN50-PVC/PRE-M','VAN63-CUIVR','VAN75-PEHD','VAN75-PVC/PRE-M','VAN90-PVC/PRE-M','VANNET-CAN16B','VANNET-LIGN16B','VAN-VOL2.5','VAN-VOL3','VARI-11','VBE-150','VHD10','VIB-CR30A','VIB-CR30B','VILC-186F','VILCON-186F','VIN-A20M','VIN-DF24T','VM-178F','VM-186F','VM-EY20','VRB20-3C','VXM10A/0.75','VXM12A/1','VXM-INO/12A1','WAH5008','WQD-0.55','WQD-0.75','XB-2025','XB-2550','XY2055-100L','XY2055-50L','YSB0502-18X400','YSB0502-18X460','YSB0502-18X600','YSB0504-30X410','YSB0604-28X500','YSB0604-28X550','YSB0604-28X620','YSB0604-32X500','YSB0604-32X550','YSB0604-32X620','ZN70','ZS1115R');
        $prevision = array(2,5,10,2,14,2,150,5,0,0,0,0,0,0,11,0,0,94,52,46,116,260,262,0,0,0,0,0,0,0,0,0,0,0,0,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,5,0,0,0,0,0,0,0,0,100,4,80,89,0,0,0,0,0,0,0,0,0,0,0,0,0,0,28,14,0,0,0,0,0,0,0,96,0,116,0,0,21,33,22,17,58,54,56,20,90,60,30,80,39,92,22,22,2,1,5,2,11,0,2,0,0,0,36,15,2,26,5,9,1,9,1,0,540,16,8,37,28,5,4,8,6,7,2,4,3,0,11,0,379,71,0,0,0,3,6,1,123,366,388,62,137,76,100,40,80,66,82,38,25,0,25,0,14,0,100,71,63,201,0,0,0,51,4,2,1,1,0,0,3,17,31,0,489,72,185,343,43,53,66,1,32,20,2,0,0,0,3,0,2,0,0,0,2,0,63,1,1,62,35,60,1,48,14,8,0,0,22,0,268,174,12,226,0,88,0,37,32,0,64,59,27,49,2,0,0,0,0,0,0,40,0,13,0,30,100,0,2,0,0,0,0,31,0,7,2,1,3,17,26,0,1,2,0,88,0,0,8,6,2,13,10,2,4,0,1,0,1,43,5,0,1,1,0,0,0,4,0,0,2,1,2,0,3,1,0,100,11,12,1,2,0,3,2,0,0,0,0,0,1,0,2,5,3,15,8,18,31,31,8,9,14,3,13,0,7,19,9,17,2,12,4,3,3,3,1,0,0,0,1,5,4,0,0,2,4,1,2,40,1,0,12,13,17,21,9,20,4,3,0,0,30,400,0,480,440,228,14,11,8,4,8,6,5,7,6,25,76,18,98,20,0,11,4,0,1,1,2,7,2,7,5,2,2,0,12,5,10,4,1,17,5,45,9,7,14,17,26,11,10,6,8,8,12,19,15,17,3,8,7,4,2,1,9,7,12,8,11,7,9,8,8,8,1,10,0,7,7,4,0,0,14,0,0,0,2,100,600,83,30,84,1,2,3,2,0,0,0,40,45,48,42,47,41,50,51,51,0,2,3,0,30,0,0,4,3,0,4,0,0,21,0,0,8,0,0,0,0,60,50,100,40,23,0,0,0,0,0,2,50,35,0,23,2,0,0,3,1,2,0,1,29,0,200,200,100,100,100,60,0,0,0,0,50,125,50,50,100,50,15,15,15,15,15,15,15,15,15,0,15,15,50,50,50,50,50,15,15,15,35,35,35,35,25,25,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,15,15,15,15,15,15,15,15,15,15,10,25,25,50,25,25,15,15,15,15,25,25,20,20,15,15,15,1,60,0,30,0,15,0,0,0,0,0,0,0,0,0,2,3925,0,1,0,0,0,0,0,0,2500,4,48,0,12,0,0,4,0,0,8,0,10,0,0,179,0,0,5,4,3,6,0,0,0,1,0,6,0,14,35,0,0,4,0,0,478,0,0,87,8,10,1,0,9,16,4,0,2,0,0,0,2,4,1,4,30,1,1,0,0,0,0,2,0,0,145,27,0,0,3,5,4,0,0,0,0,0,0,0,0,0,105,7,5,0,0,0,0,0,2,60,13,2,83,7,1,270,131,13,75,7,32,18,27,16,5,83,26,22,8,125,37,105,8,39,18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,3,2,0,12,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,0,3,0,41,0,2,44,0,68,19,0,0,2,0,11,34,0,9,18,0,150,0,0,0,0,0,0,0,0,200,0,145,3,6,6,3,3,0,114,0,11,1,9,0,2,5,230,320,310,0,7,5,49,2,12,15,10,2,12,0,0,0,7,13,9,5,15,3,11,3,2,1,4,15,5,0,0,1,1,0,0,0,0,31,14,60,500,2,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,14,0,0,0,0,188,174,24,23,0,21,0,0,0,6,0,0,0,0,0,0,0,74,22,1,0,0,0,3,6,7,0,0,0,0,0,0,1,1,6,1,0,0,0,5,0,0,0,0,4,0,3,0,0,0,0,0,1397,0,0,28,216,203,288,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,264,0,0,0,0,73,886,0,0,0,0,0,0,0,0,0,0,331,23,124,58,29,88,3,1,0,0,0,0,0,0,0,0,1,1,0,0,5,0,7,80,5,58,30,300,9,6,40,0,20,6,2,60,5,80,4,8,16,10,4,0,63,0,55,0,19,13,0,74,10,1,118,8,1,25,5,5,8,17,12,26,10,9,19,0,8,18,0,4,7,0,62,0,1,0,116,0,0,0,0,3,1,0,0,0,0,0,4,20,0,0,1,1,0,0,41,0,160,240,8,321,16,0,0,0,12,3,0,0,0,0,1,0,0,3,2,0,0,4,0,11,34,50,7,7,242,15,27,0,0,25,1,3,2,1,0,1,4,0,0,36,265,0,0,0,0,0,0,0,0,0,0,0,0,0,139,0,0,0,1,1,0,0,0,0,0,0,0,0,50,0,23,0,1,1,0,0,0,113,114,140,52,114,108,98,150,49,108,94,0,2,1,0,0,0,0,0,280,0,0,0,3,4,3,0,0,0,0,12,5,47,154,30,40,0,0,2,1,3,3,5,5,1,6,3,0,0,0,0,1,3,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,207,8,7,1,0,0,0,0,0,0,13,0,93,0,961,0,11,4,0,6,10,10,4,8,0,0,0,0,0,0,0,0,0,0,0,60,11,0,0,0,1,0,0,0,1,0,45,4,14,15,0,0,0,70,60,25,45,228,52,0,192,97,42,15,15,53,35,40,0);
        $articlesID = array();
        for($i=0;$i<count($articles);$i++){
            $a = Article::where('code',$articles[$i])->first();
            if($a){
                $pre = new Prevision;
                $pre->famille_id = $a->famille_id;
                $pre->article_id = $a->id;
                $pre->methode_id = 2;
                $pre->date = '2021-09-01';
                $pre->prevision = $prevision[$i];
                $pre->save();
            }
            array_push($articlesID,$a->id);
        }
        return "done";
    }
















    public function getAll()
    {
        $clients = Client::all();
        $familles = Famille::all();
        $zones = Zone::all();
        $taux = Taux::all();
        $data = [
            "familles"=>$familles,
            "zones"=>$zones,
            "clients"=>$clients,
            "taux"=>$taux
        ];

        return $data;
    }

    public function getAlpha()
    {
        
        $alpha = Alpha::where('active',1)->get();

        return $alpha;
    }

     public function editAlpha(Request $request)
    {
        $a = Alpha::where('id',1)->first();
        $a->alpha = $request->alpha;
        $a->save();
        return $request->alpha;
    }



    public function ListeFamilles()
    {
       $Familles = DB::connection('sqlsrv')->select('SELECT DISTINCT ARTCOLLECTION FROM dbo.ARTICLES');
       return $Familles;
    } 
    public function ListeArticles()
    {
       $Articles = DB::connection('sqlsrv')->select('SELECT ARTID,ARTCODE,ARTDESIGNATION,ARTCOLLECTION FROM dbo.ARTICLES');
       return $Articles;
    }

    public function ListeZones()
    {
       $Zones = DB::connection('sqlsrv')->select('SELECT DISTINCT CLIVILLE FROM dbo.V_STATISTIQUE_VENTE');
       return $Zones;
    }

    // Listes des Ventes
    
    public function ListeVentes()
    {
        $Ventes = DB::connection('sqlsrv')->select("SELECT TOP 50 PLVID,ARTID,PCVNUM,DATELIGNE,PLVQTEUS,CLIVILLE,MNTNETHT FROM dbo.V_STATISTIQUE_VENTE WHERE (PCVNUM LIKE '%BC%')");
        return $Ventes;
    }

    public function ListeVenteslst()
    {
        $Ventes = DB::connection('sqlsrv')->select("SELECT TOP 150 PLVID,PCVNUM,ARTID,PLVDATE,PLVQTE FROM dbo.V_LST_PIECEVENTELIGNES WHERE JORCODE = 'VE' AND PITCODE = 'F'");
        return $Ventes;
    }

    public function ListeVentesplanifie()
    {
        $Ventes = DB::connection('sqlsrv')->select("SELECT TOP 150 PLVID,PCVNUM,ARTID,PLVDATE,PLVQTE FROM dbo.V_LST_PIECEVENTELIGNES WHERE JORCODE = 'VE' AND ((PINID = 91) OR (PINID = 146)) AND PLVTYPE = 'L' AND PCVISSOLDE <> 'O'");
        return $Ventes;
    }
   


    // Listes des Achats
    public function ListeAchats()
    {
        $Ventes = DB::connection('sqlsrv')->select("SELECT TOP 150 PCANUM,ARTID,PLADATE,PLAQTE FROM dbo.V_LST_PIECEACHATLIGNES WHERE PINID=60");
        return $Ventes;
    }
    public function ListeAchatsplanifie()
    {
        $Ventes = DB::connection('sqlsrv')->select("SELECT TOP 150 PCANUM,ARTID,PLADATE,PLAQTE FROM dbo.V_LST_PIECEACHATLIGNES WHERE PINCODE IN ('RFQ','or','of','cof') AND EXEID=24");
        return $Ventes;
    }


    //Listes des stocks
     public function ListeStocks()
    {
        $Stocks = DB::connection('sqlsrv')->select("SELECT ARTID,STOCKREEL FROM dbo.V_STOCK_ARTICLES");
        // $Stocks = DB::connection('sqlsrv')->select("SELECT TOP 4 ARTID,STOCKREEL FROM dbo.V_STOCK_ARTICLES");
        return $Stocks;
        // return $start = new Carbon('last day of last month');
        // return Carbon::now();
    }

    public function ListeStocks2()
    {
        $Stocks = DB::connection('sqlsrv')->select("SELECT ARTID,DEPID,ARDSTOCKREEL FROM dbo.ARTDEPOT WHERE DEPID = 2 OR DEPID = 3 OR DEPID = 17 OR DEPID = 19");
        // $Stocks = DB::connection('sqlsrv')->select("SELECT TOP 4 ARTID,STOCKREEL FROM dbo.V_STOCK_ARTICLES");
        return $Stocks;
        // return $start = new Carbon('last day of last month');
        // return Carbon::now();
    }

    //Ajout données au DB Reception

    public function TransfertFamille()
    {
       $Familles = $this->ListeFamilles();
       $sum = 0;
       foreach ($Familles as $item) {
           $checkFamille = Famille::where('famille',$item->ARTCOLLECTION)->first(); 
           if(!$checkFamille){
                $famille = new Famille;
                $famille->famille = $item->ARTCOLLECTION;
                $famille->tauxsecurite_id = 1;
                $famille->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Famille';
       $log->description = $sum." Famille(s) Ajouté(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Famille(s) Ajoutée(s) à la base de donnée.";
    } 

     public function TransfertZone()
    {
       $Zones = $this->ListeZones();
       $sum = 0;
       foreach ($Zones as $item) {
           $checkZone = Zone::where('zone',$item->CLIVILLE)->first(); 
           if(!$checkZone){
                $zone = new Zone;
                $zone->zone = $item->CLIVILLE;
                $zone->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Zone';
       $log->description = $sum." Zone(s) Ajouté(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Zone(s) Ajouté(s) à la base de donnée.";
    } 

    public function TransfertArticle()
    {
       $Articles = $this->ListeArticles();
       $sum = 0;
       foreach ($Articles as $item) {
           $checkArticle = Article::where('code',$item->ARTCODE)->first(); 
           if(!$checkArticle){
                $famille_id = Famille::where('famille',$item->ARTCOLLECTION)->first();
                $article = new Article;
                $article->ARTID = $item->ARTID;
                $article->code = $item->ARTCODE;
                $article->designation = $item->ARTDESIGNATION;
                $article->famille_id = $famille_id->id;
                $article->classe = "A";
                $article->strategie = "MTS";
                $article->tauxsecurite_id = 1;
                $article->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Article';
       $log->description = $sum." Article(s) Ajouté(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Article(s) Ajouté(s) à la base de donnée.";
    } 

      public function TransfertCommande()
    {
       $Ventes = $this->ListeVentes();
       $sum = 0;
       foreach ($Ventes as $item) {
           $checkVente = Commande::where('CMDID',$item->PLVID)->first(); 
           if(!$checkVente){

                $Article = Article::where('ARTID',$item->ARTID)->first();
                $Zone = Zone::where('zone',$item->CLIVILLE)->first();

                $vente = new Commande;
                $vente->article_id = $Article->id;
                $vente->famille_id = $Article->famille_id;
                $vente->CMDID = $item->PLVID;
                $vente->zone_id = $Zone->id;
                $vente->quantite = $item->PLVQTEUS;
                $vente->montant = $item->MNTNETHT;
                $vente->date_vente = $item->DATELIGNE;
                $vente->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Commande';
       $log->description = $sum." Commande(s) Ajouté(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Vente(s) Ajoutée(s) à la base de donnée.";
    } 
    
    // Nouvelle Functions
    public function TransfertCommande2()
    {
       $Ventes = $this->ListeVenteslst();
       $sum = 0;
       foreach ($Ventes as $item) {
            $Article = Article::where('ARTID',$item->ARTID)->first();
           if($Article){
                $Zone = Zone::where('zone',$item->CLIVILLE)->first();
                $zonid = 1;
                if($Zone){
                    $zonid = $Zone->id;
                }else{
                    $zonid = 1;
                }
                $vente = new Commande;
                $vente->article_id = $Article->id;
                $vente->famille_id = $Article->famille_id;
                $vente->zone_id = $zonid;
                $vente->quantite = $item->PLVQTE;
                $vente->date_vente = $item->PLVDATE;
                $vente->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Commande';
       $log->description = $sum." Commande(s) Ajoutée(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Vente(s) Ajoutée(s) à la base de donnée.";
    } 

    public function TransfertCommandePlanifie()
    {
       $Ventes = $this->ListeVentesplanifie();
       $sum = 0;
       foreach ($Ventes as $item) {
            $Article = Article::where('ARTID',$item->ARTID)->first();
           if($Article){
                $Zone = Zone::where('zone',$item->CLIVILLE)->first();
                $zonid = 1;
                if($Zone){
                    $zonid = $Zone->id;
                }else{
                    $zonid = 1;
                }
                $vente = new CmdPlan;
                $vente->article_id = $Article->id;
                $vente->famille_id = $Article->famille_id;
                $vente->zone_id = $zonid;
                $vente->quantite = $item->PLVQTE;
                $vente->date_vente = $item->PLVDATE;
                $vente->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Commande Planifiée';
       $log->description = $sum." Commande(s) Planifiée(s) Ajoutée(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Vente(s) Planifié Ajoutée(s) à la base de donnée.";
    } 

    // Transferts Achats
    public function TransfertAchats()
    {
       $Achats = $this->ListeAchats();
       $sum = 0;
       foreach ($Achats as $item) {
            $Article = Article::where('ARTID',$item->ARTID)->first();
           if($Article){
                $Zone = Zone::where('zone',$item->CLIVILLE)->first();
                $zonid = 1;
                if($Zone){
                    $zonid = $Zone->id;
                }else{
                    $zonid = 1;
                }
                $achat = new Achat;
                $achat->article_id = $Article->id;
                $achat->famille_id = $Article->famille_id;
                $achat->zone_id = $zonid;
                $achat->quantite = $item->PLAQTE;
                $achat->date = $item->PLADATE;
                $achat->month = date_format($item->PLADATE, 'm-Y');;
                $achat->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Achat';
       $log->description = $sum." Achat(s) Ajoutée(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Vente(s) Planifié Ajoutée(s) à la base de donnée.";
    } 

    public function TransfertAchatsPlanifie()
    {
       $Achats = $this->ListeAchatsplanifie();
       $sum = 0;
       foreach ($Achats as $item) {
            $Article = Article::where('ARTID',$item->ARTID)->first();
           if($Article){
                $Zone = Zone::where('zone',$item->CLIVILLE)->first();
                $zonid = 1;
                if($Zone){
                    $zonid = $Zone->id;
                }else{
                    $zonid = 1;
                }
                $achat = new Achatplan;
                $achat->article_id = $Article->id;
                $achat->famille_id = $Article->famille_id;
                $achat->zone_id = $zonid;
                $achat->quantite = $item->PLAQTE;
                $achat->date = $item->PLADATE;
                $achat->month = date_format($item->PLADATE, 'm-Y');;
                $achat->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Achat Planifiée';
       $log->description = $sum." Achat(s) Planifiée(s) Ajoutée(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Vente(s) Planifié Ajoutée(s) à la base de donnée.";
    } 


    public function testo()
    {
        $date = new DateTime('2014-12-31');
        $date->modify('last day of second month');
        echo $date->format('Y-m-d');
    }



     public function TransfertStock()
    {
       $Stocks = $this->ListeStocks();
       $sum = 0;
       foreach ($Stocks as $item) {
           $Article = Article::where('ARTID',$item->ARTID)->first();
           if($Article){
                $stock = new Stock;
                $stock->article_id = $Article->id;
                $stock->famille_id = $Article->famille_id;
                $stock->quantite = $item->STOCKREEL;
                $stock->date = Carbon::now();
                $stock->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Stock';
       $log->description = $sum." Stock(s) Ajouté(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Stock(s) Ajouté(s) à la base de donnée.";
    } 

    public function TransfertStock2()
    {
       $Stocks = $this->ListeStocks2();
       $sum = 0;
       foreach ($Stocks as $item) {
           $Article = Article::where('ARTID',$item->ARTID)->first();
           if($Article){
                $stock = new Stock;
                $stock->article_id = $Article->id;
                $stock->famille_id = $Article->famille_id;
                $stock->quantite = $item->ARDSTOCKREEL;
                $stock->date = Carbon::now();
                $stock->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Stock';
       $log->description = $sum." Stock(s) Ajouté(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Stock(s) Ajouté(s) à la base de donnée.";
    } 


    //interfaçage transfert Ventes Grande Liste CSV
    public function getArticlesList()
    {
        $articles = Article::All();
        $data = array();
        $dataf = array();
        $a = array();
        $b = array();
        for($i = 0;$i<Count($articles);$i++){
            $data[$articles[$i]->id] = $articles[$i]->ARTID;
            array_push($a,$articles[$i]->famille_id);
            array_push($b,$articles[$i]->id);
        }
        // echo array_search("4146",$data);
        return [
            'articles'=>$data,
            'a'=>$a,
            'b'=>$b
        ];
    }

    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;
        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 0, $delimiter)) !== false) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }


    public function importVentes()
    {
        $file = public_path('ventes.csv');
        $importedCSV = $this->csvToArray($file);
        $art = $this->getArticlesList();
        $articleList = $art['articles'];
        $familleList = $art['a'];
        $articleidList = $art['b'];
        $finalData = array();
        for($i = 0;$i<Count($importedCSV);$i++){
            if($importedCSV[$i]['artid']){
                $articleID = array_search($importedCSV[$i]['artid'],$articleList);
                $articleIndex = array_search($articleID, $art['b']);
                if($articleID){
                    
                    $subData['id']=$articleID;
                    $subData['familleid']=$familleList[$articleIndex];
                    $subData['artid']=$importedCSV[$i]['artid'];
                    $subData['date']=$importedCSV[$i]['date'];
                    $subData['dateform']=date("Y-m-d", strtotime($importedCSV[$i]['date']));
                    $subData['qte']=round($importedCSV[$i]['qte']);
                    array_push($finalData,$subData);
                }
            }
        }
        $sum = 0;
        for($i = 0;$i<Count($finalData);$i++){
            $commande = new Commande;
            $commande->article_id = $finalData[$i]['id'];
            $commande->famille_id = $finalData[$i]['familleid'];
            $commande->date_vente = $finalData[$i]['dateform'];
            $commande->quantite = $finalData[$i]['qte'];
            $commande->zone_id = 1;
            $commande->save();
            $sum = $sum + 1;
        }

        return $sum." Vente(s) Ajoutée(s).";
    } 

    public function importVentesplanifi()
    {
        $file = public_path('ventesplan.csv');
        $importedCSV = $this->csvToArray($file);
        $art = $this->getArticlesList();
        $articleList = $art['articles'];
        $familleList = $art['a'];
        $articleidList = $art['b'];
        $finalData = array();
        for($i = 0;$i<Count($importedCSV);$i++){
            if($importedCSV[$i]['artid']){
                $articleID = array_search($importedCSV[$i]['artid'],$articleList);
                $articleIndex = array_search($articleID, $art['b']);
                if($articleID){
                    
                    $subData['id']=$articleID;
                    $subData['familleid']=$familleList[$articleIndex];
                    $subData['artid']=$importedCSV[$i]['artid'];
                    $subData['date']=$importedCSV[$i]['date'];
                    $subData['dateform']=date("Y-m-d", strtotime($importedCSV[$i]['date']));
                    $subData['qte']=round($importedCSV[$i]['qte']);
                    array_push($finalData,$subData);
                }
            }
        }
        $sum = 0;
        for($i = 0;$i<Count($finalData);$i++){
            $commande = new CmdPlan;
            $commande->article_id = $finalData[$i]['id'];
            $commande->famille_id = $finalData[$i]['familleid'];
            $commande->date_vente = $finalData[$i]['dateform'];
            $commande->date_plan = $finalData[$i]['dateform'];
            $commande->quantite = $finalData[$i]['qte'];
            $commande->zone_id = 1;
            $commande->save();
            $sum = $sum + 1;
        }

        return $sum." Vente(s) planifié Ajoutée(s).";
    } 

    public function importAchats()
    {
        $file = public_path('achats.csv');
        $importedCSV = $this->csvToArray($file);
        $art = $this->getArticlesList();
        $articleList = $art['articles'];
        $familleList = $art['a'];
        $articleidList = $art['b'];
        $finalData = array();
        for($i = 0;$i<Count($importedCSV);$i++){
            if($importedCSV[$i]['artid']){
                $articleID = array_search($importedCSV[$i]['artid'],$articleList);
                $articleIndex = array_search($articleID, $art['b']);
                if($articleID){
                    
                    $subData['id']=$articleID;
                    $subData['familleid']=$familleList[$articleIndex];
                    $subData['artid']=$importedCSV[$i]['artid'];
                    $subData['date']=$importedCSV[$i]['date'];
                    $subData['dateform']=date("Y-m-d", strtotime($importedCSV[$i]['date']));
                    $subData['month']=date("m-Y", strtotime($importedCSV[$i]['date']));
                    $subData['qte']=round($importedCSV[$i]['qte']);
                    array_push($finalData,$subData);
                }
            }
        }
        $sum = 0;
        for($i = 0;$i<Count($finalData);$i++){
            $achat = new Achat;
            $achat->article_id = $finalData[$i]['id'];
            $achat->famille_id = $finalData[$i]['familleid'];
            $achat->date = $finalData[$i]['dateform'];
            $achat->month = $finalData[$i]['month'];
            $achat->quantite = $finalData[$i]['qte'];
            $achat->save();
            $sum = $sum + 1;
        }

        return $sum." Achat(s) Ajoutée(s).";
    } 

    public function importAchatsplan()
    {
        $file = public_path('achatsplan.csv');
        $importedCSV = $this->csvToArray($file);
        $art = $this->getArticlesList();
        $articleList = $art['articles'];
        $familleList = $art['a'];
        $articleidList = $art['b'];
        $finalData = array();
        for($i = 0;$i<Count($importedCSV);$i++){
            if($importedCSV[$i]['artid']){
                $articleID = array_search($importedCSV[$i]['artid'],$articleList);
                $articleIndex = array_search($articleID, $art['b']);
                if($articleID){
                    
                    $subData['id']=$articleID;
                    $subData['familleid']=$familleList[$articleIndex];
                    $subData['artid']=$importedCSV[$i]['artid'];
                    $subData['date']=$importedCSV[$i]['date'];
                    $subData['dateform']=date("Y-m-d", strtotime($importedCSV[$i]['date']));
                    $datex = date("Y-m-d", strtotime($importedCSV[$i]['date']));
                    $date = new DateTime($datex);
                    $date->modify('last day of second month');
                    $subData['datereception']= $date->format('Y-m-d');
                    $subData['qte']=round($importedCSV[$i]['qte']);
                    array_push($finalData,$subData);
                }
            }
        }
        $sum = 0;
        for($i = 0;$i<Count($finalData);$i++){
            $achat = new Achatplan;
            $achat->article_id = $finalData[$i]['id'];
            $achat->famille_id = $finalData[$i]['familleid'];
            $achat->date_achat = $finalData[$i]['dateform'];
            $achat->date_reception = $finalData[$i]['datereception'];
            $achat->quantite = $finalData[$i]['qte'];
            $achat->save();
            $sum = $sum + 1;
        }

        return $sum." Achat(s) planifiée(s) Ajoutée(s).";
    } 

    public function tested(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'title' => 'required|airobo',
        ]);

        if ($validator->fails()) {
            return ['data'=>null,'errors'=>$validator->errors()];
        }


        return "okey";
    }


    
    public function ListeVenteslstDaily()
    {
        
        $Ventes = DB::connection('sqlsrv')->select("SELECT PLVID,PCVNUM,ARTID,PLVDATE,PLVQTE FROM dbo.V_LST_PIECEVENTELIGNES  WHERE JORCODE = 'VE' AND PITCODE = 'F' AND ((PINID = 5) OR (PINID = 45) OR (PINID = 147) OR (PINID = 149)) AND EXEID>=18 AND ARTID IS NOT NULL AND CAST(PLVDATE AS date) >=CONVERT(varchar,GETDATE()) ORDER BY PLVDATE DESC");
        return $Ventes;
		//BL - AVOIR
    }
}
