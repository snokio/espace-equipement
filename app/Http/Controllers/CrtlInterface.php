<?php

namespace App\Http\Controllers;

use App\Models\Achat;
use App\Models\Achatplan;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Famille;
use App\Models\Zone;
use App\Models\Client;
use App\Models\Taux;
use App\Models\Alpha;
use App\Models\Commande;
use App\Models\Stock;
use App\Models\Article;
use App\Models\CmdPlan;
use DB;
use DateTime;
use Carbon\Carbon;
use App\Models\Log;

use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function getAll()
    {
        $clients = Client::all();
        $familles = Famille::all();
        $zones = Zone::all();
        $taux = Taux::all();
        $data = [
            "familles"=>$familles,
            "zones"=>$zones,
            "clients"=>$clients,
            "taux"=>$taux
        ];

        return $data;
    }

    public function getAlpha()
    {
        
        $alpha = Alpha::where('active',1)->get();

        return $alpha;
    }

     public function editAlpha(Request $request)
    {
        $a = Alpha::where('id',1)->first();
        $a->alpha = $request->alpha;
        $a->save();
        return $request->alpha;
    }



    public function ListeFamilles()
    {
       $Familles = DB::connection('sqlsrv')->select('SELECT DISTINCT ARTCOLLECTION FROM dbo.ARTICLES');
       return $Familles;
    } 
    public function ListeArticles()
    {
       $Articles = DB::connection('sqlsrv')->select('SELECT ARTID,ARTCODE,ARTDESIGNATION,ARTCOLLECTION FROM dbo.ARTICLES');
       return $Articles;
    }

    public function ListeZones()
    {
       $Zones = DB::connection('sqlsrv')->select('SELECT DISTINCT CLIVILLE FROM dbo.V_STATISTIQUE_VENTE');
       return $Zones;
    }

    // Listes des Ventes
    
    public function ListeVentes()
    {
        $Ventes = DB::connection('sqlsrv')->select("SELECT TOP 50 PLVID,ARTID,PCVNUM,DATELIGNE,PLVQTEUS,CLIVILLE,MNTNETHT FROM dbo.V_STATISTIQUE_VENTE WHERE (PCVNUM LIKE '%BC%')");
        return $Ventes;
    }

    public function ListeVenteslstDaily()
    {
        $date = "2021-09-11";
        $Ventes = DB::connection('sqlsrv')->select("SELECT PLVID,PCVNUM,ARTID,PLVDATE,PLVQTE FROM dbo.V_LST_PIECEVENTELIGNES  WHERE JORCODE = 'VE' AND PITCODE = 'F' AND ((PINID = 5) OR (PINID = 45) OR (PINID = 147) OR (PINID = 149)) AND EXEID>=18 AND ARTID IS NOT NULL AND CAST(PLVDATE AS date)=CONVERT(varchar,GETDATE()) ORDER BY PLVDATE DESC");
        return $Ventes;
		
    }
 public function ListeVenteslst()
    {
        $Ventes = DB::connection('sqlsrv')->select("SELECT PLVID,PCVNUM,ARTID,PLVDATE,PLVQTE FROM dbo.V_LST_PIECEVENTELIGNES  WHERE JORCODE = 'VE' AND PITCODE = 'F' AND ((PINID = 5) OR (PINID = 45) OR (PINID = 147) OR (PINID = 149)) AND EXEID>=18 AND ARTID IS NOT NULL AND PLVDATE = '2021-15-09' ORDER BY PLVDATE DESC");
        return $Ventes;
        //BL - AVOIR
    }

    public function ListeVentesplanifieDaily()
    {
        $Ventes = DB::connection('sqlsrv')->select("SELECT PLVID,PCVNUM,ARTID,PLVDATE,PLVQTE FROM dbo.V_LST_PIECEVENTELIGNES WHERE JORCODE = 'VE' AND ((PINID = 91) OR (PINID = 146)) AND PLVTYPE = 'L' AND PCVISSOLDE <> 'O' AND ARTID IS NOT NULL  AND CAST(PLVDATE AS date)=CONVERT(varchar,GETDATE()) ORDER BY PLVID DESC");
        return $Ventes;
		//CDECLI-OK + ODLNET
    }
    public function ListeVentesplanifie()
    {
        $Ventes = DB::connection('sqlsrv')->select("SELECT TOP 150 PLVID,PCVNUM,ARTID,PLVDATE,PLVQTE FROM dbo.V_LST_PIECEVENTELIGNES WHERE JORCODE = 'VE' AND ((PINID = 91) OR (PINID = 146)) AND PLVTYPE = 'L' AND PCVISSOLDE <> 'O'");
        return $Ventes;
        //CDECLI-OK + ODLNET
    }
   


    // Listes des Achats
    public function ListeAchats()
    {
        $Ventes = DB::connection('sqlsrv')->select("SELECT TOP 150 PCANUM,ARTID,PLADATE,PLAQTE FROM dbo.V_LST_PIECEACHATLIGNES WHERE PINID=60");
        return $Ventes;
		//COF
    }
    public function ListeAchatsplanifieDaily()
    {
        $Ventes = DB::connection('sqlsrv')->select("SELECT PCANUM,ARTID,PLADATE,PLAQTE FROM dbo.V_LST_PIECEACHATLIGNES WHERE PINCODE IN ('RFQ','or','of','cof') AND EXEID=24 AND ARTID IS NOT NULL  AND CAST(PLADATE AS date)=CONVERT(varchar,GETDATE()-5) ORDER BY PLADATE ASC");
        return $Ventes;
    }

     public function ListeAchatsplanifie()
    {
        $Ventes = DB::connection('sqlsrv')->select("SELECT TOP 150 PCANUM,ARTID,PLADATE,PLAQTE FROM dbo.V_LST_PIECEACHATLIGNES WHERE PINCODE IN ('RFQ','or','of','cof') AND EXEID=24");
        return $Ventes;
    }


    //Listes des stocks
     public function ListeStocks()
    {
        $Stocks = DB::connection('sqlsrv')->select("SELECT ARTID,STOCKREEL FROM dbo.V_STOCK_ARTICLES");
        // $Stocks = DB::connection('sqlsrv')->select("SELECT TOP 4 ARTID,STOCKREEL FROM dbo.V_STOCK_ARTICLES");
        return $Stocks;
        // return $start = new Carbon('last day of last month');
        // return Carbon::now();
    }

    public function ListeStocks2()
    {
        $Stocks = DB::connection('sqlsrv')->select("SELECT ARTID,DEPID,ARDSTOCKREEL,ARDLASTDATEIN,ARDLASTDATEOUT FROM dbo.ARTDEPOT WHERE DEPID = 2 OR DEPID = 3 OR DEPID = 15 OR DEPID = 17 OR DEPID = 19 OR DEPID = 24 OR DEPID = 27");
        // $Stocks = DB::connection('sqlsrv')->select("SELECT TOP 4 ARTID,STOCKREEL FROM dbo.V_STOCK_ARTICLES");
        return $Stocks;
        // return $start = new Carbon('last day of last month');
        // return Carbon::now();
    }

    //Ajout données au DB Reception

    public function TransfertFamille()
    {
       $Familles = $this->ListeFamilles();
       $sum = 0;
       foreach ($Familles as $item) {
		   if($item->ARTCOLLECTION != null){
			   $checkFamille = Famille::where('famille',$item->ARTCOLLECTION)->first(); 
			   if(!$checkFamille){
					$famille = new Famille;
					$famille->famille = $item->ARTCOLLECTION;
					$famille->tauxsecurite_id = 1;
					$famille->save();
					$sum = $sum + 1;
			   }
		   }
           
       }
       $log = new Log;
       $log->task = 'Ajout Famille';
       $log->description = $sum." Famille(s) Ajouté(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Famille(s) Ajoutée(s) à la base de donnée.";
    } 

     public function TransfertZone()
    {
       $Zones = $this->ListeZones();
       $sum = 0;
       foreach ($Zones as $item) {
           $checkZone = Zone::where('zone',$item->CLIVILLE)->first(); 
           if(!$checkZone){
                $zone = new Zone;
                $zone->zone = $item->CLIVILLE;
                $zone->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Zone';
       $log->description = $sum." Zone(s) Ajouté(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Zone(s) Ajouté(s) à la base de donnée.";
    } 

    public function TransfertArticle()
    {
       $Articles = $this->ListeArticles();
       $sum = 0;
       foreach ($Articles as $item) {
		   if($item->ARTCODE != null){
			   $checkArticle = Article::where('code',$item->ARTCODE)->first(); 
           if(!$checkArticle){
			   if($item->ARTCOLLECTION == null){
				$famille_id = Famille::where('famille','Autre')->first();   
			   }else{
				$famille_id = Famille::where('famille',$item->ARTCOLLECTION)->first();
			   }
                $article = new Article;
                $article->ARTID = $item->ARTID;
				$article->code = $item->ARTCODE;
                $article->designation = $item->ARTDESIGNATION;
                $article->famille_id = $famille_id->id;
                $article->classe = "A";
                $article->strategie = "MTS";
                $article->tauxsecurite_id = 1;
                $article->save();
                $sum = $sum + 1;
           }
		   }
           
       }
       $log = new Log;
       $log->task = 'Ajout Article';
       $log->description = $sum." Article(s) Ajouté(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Article(s) Ajouté(s) à la base de donnée.";
    } 

      public function TransfertCommande()
    {
       $Ventes = $this->ListeVentes();
       $sum = 0;
       foreach ($Ventes as $item) {
           $checkVente = Commande::where('CMDID',$item->PLVID)->first(); 
           if(!$checkVente){

                $Article = Article::where('ARTID',$item->ARTID)->first();
                $Zone = Zone::where('zone',$item->CLIVILLE)->first();

                $vente = new Commande;
                $vente->article_id = $Article->id;
                $vente->famille_id = $Article->famille_id;
                $vente->CMDID = $item->PLVID;
                $vente->zone_id = $Zone->id;
                $vente->quantite = $item->PLVQTEUS;
                $vente->montant = $item->MNTNETHT;
                $vente->date_vente = $item->DATELIGNE;
                $vente->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Commande';
       $log->description = $sum." Commande(s) Ajouté(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Vente(s) Ajoutée(s) à la base de donnée.";
    } 
    
    // Nouvelle Functions
    public function TransfertCommande2()
    {
       $Ventes = $this->ListeVenteslst();
       $sum = 0;
       foreach ($Ventes as $item) {
            $Article = Article::where('ARTID',$item->ARTID)->first();
           if($Article){
                $vente = new Commande;
                $vente->article_id = $Article->id;
                $vente->famille_id = $Article->famille_id;
                
                $vente->quantite = $item->PLVQTE;
                $vente->date_vente = $item->PLVDATE;
                $vente->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Commande';
       $log->description = $sum." Commande(s) Ajoutée(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Vente(s) Ajoutée(s) à la base de donnée.";
    } 

    public function TransfertCommandePlanifie()
    {
       $Ventes = $this->ListeVentesplanifie();
       $sum = 0;
       foreach ($Ventes as $item) {
            $Article = Article::where('ARTID',$item->ARTID)->first();
           if($Article){
                $zonid = 1;
                $vente = new CmdPlan;
                $vente->article_id = $Article->id;
                $vente->famille_id = $Article->famille_id;
                $vente->zone_id = $zonid;
                $vente->quantite = $item->PLVQTE;
                $vente->date_vente = $item->PLVDATE;
                $vente->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Commande Planifiée';
       $log->description = $sum." Commande(s) Planifiée(s) Ajoutée(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Vente(s) Planifié Ajoutée(s) à la base de donnée.";
    } 

    // Transferts Achats
    public function TransfertAchats()
    {
       $Achats = $this->ListeAchats();
       $sum = 0;
       foreach ($Achats as $item) {
            $Article = Article::where('ARTID',$item->ARTID)->first();
           if($Article){
                $Zone = Zone::where('zone',$item->CLIVILLE)->first();
                $zonid = 1;
                if($Zone){
                    $zonid = $Zone->id;
                }else{
                    $zonid = 1;
                }
                $achat = new Achat;
                $achat->article_id = $Article->id;
                $achat->famille_id = $Article->famille_id;
                $achat->zone_id = $zonid;
                $achat->quantite = $item->PLAQTE;
                $achat->date = $item->PLADATE;
                $achat->month = date_format($item->PLADATE, 'm-Y');;
                $achat->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Achat';
       $log->description = $sum." Achat(s) Ajoutée(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Vente(s) Planifié Ajoutée(s) à la base de donnée.";
    } 

    public function TransfertAchatsPlanifie()
    {
       $Achats = $this->ListeAchatsplanifie();
       $sum = 0;
       foreach ($Achats as $item) {
            $Article = Article::where('ARTID',$item->ARTID)->first();
           if($Article){
                $zonid = 1;
				$d = new DateTime($item->PLADATE);
				$d->modify('last day of second month');
                $achat = new Achatplan;
                $achat->article_id = $Article->id;
                $achat->famille_id = $Article->famille_id;
                $achat->quantite = round($item->PLAQTE);
                $achat->date_achat = date('Y-m-d', strtotime($item->PLADATE));
				$achat->date_reception = $d->format('Y-m-d');
                $achat->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Achat Planifiée';
       $log->description = $sum." Achat(s) Planifiée(s) Ajoutée(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Vente(s) Planifié Ajoutée(s) à la base de donnée.";
    } 


    public function testo()
    {
        $date = new DateTime('2014-12-31');
        $date->modify('last day of second month');
        echo $date->format('Y-m-d');
    }



     public function TransfertStock()
    {
       $Stocks = $this->ListeStocks();
       $sum = 0;
       foreach ($Stocks as $item) {
           $Article = Article::where('ARTID',$item->ARTID)->first();
           if($Article){
                $stock = new Stock;
                $stock->article_id = $Article->id;
                $stock->famille_id = $Article->famille_id;
                $stock->quantite = $item->STOCKREEL;
                $stock->date = Carbon::now();
                $stock->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Stock';
       $log->description = $sum." Stock(s) Ajouté(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Stock(s) Ajouté(s) à la base de donnée.";
    } 

    public function TransfertStock2()
    {
       $Stocks = $this->ListeStocks2();
       $sum = 0;
       foreach ($Stocks as $item) {
           $Article = Article::where('ARTID',$item->ARTID)->first();
           if($Article){
                $stock = new Stock;
                $stock->article_id = $Article->id;
                $stock->famille_id = $Article->famille_id;
                $stock->quantite = round($item->ARDSTOCKREEL);
                $stock->date = Carbon::now();
                $stock->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Stock';
       $log->description = $sum." Stock(s) Ajouté(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Stock(s) Ajouté(s) à la base de donnée.";
    } 
	
	
	
	
	 public function getArticlesList()
    {
        $articles = Article::All();
        $data = array();
        $dataf = array();
        $a = array();
        $b = array();
        for($i = 0;$i<Count($articles);$i++){
            $data[$articles[$i]->id] = $articles[$i]->ARTID;
            array_push($a,$articles[$i]->famille_id);
            array_push($b,$articles[$i]->id);
        }
        // echo array_search("4146",$data);
        return [
            'articles'=>$data,
            'a'=>$a,
            'b'=>$b
        ];
    }

    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;
        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 0, $delimiter)) !== false) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }
	
	 public function importVentes()
    {
        $file = public_path('ventes.csv');
        $importedCSV = $this->csvToArray($file);
        $art = $this->getArticlesList();
        $articleList = $art['articles'];
        $familleList = $art['a'];
        $articleidList = $art['b'];
        $finalData = array();
        for($i = 0;$i<Count($importedCSV);$i++){
            if($importedCSV[$i]['artid']){
                $articleID = array_search($importedCSV[$i]['artid'],$articleList);
                $articleIndex = array_search($articleID, $art['b']);
                if($articleID){
                    
                    $subData['id']=$articleID;
                    $subData['familleid']=$familleList[$articleIndex];
                    $subData['artid']=$importedCSV[$i]['artid'];
                    $subData['date']=$importedCSV[$i]['date'];
                    $subData['dateform']=date("Y-m-d", strtotime($importedCSV[$i]['date']));
                    $subData['qte']=round($importedCSV[$i]['qte']);
                    array_push($finalData,$subData);
                }
            }
        }
        $sum = 0;
        for($i = 0;$i<Count($finalData);$i++){
            $commande = new Commande;
            $commande->article_id = $finalData[$i]['id'];
            $commande->famille_id = $finalData[$i]['familleid'];
            $commande->date_vente = $finalData[$i]['dateform'];
            $commande->quantite = $finalData[$i]['qte'];
            $commande->zone_id = 1;
            $commande->save();
            $sum = $sum + 1;
        }

        return $sum." Vente(s) Ajoutée(s).";
    } 


     public function TransfertCommandeDaily()
    {
       $Ventes = $this->ListeVenteslstDaily();
       $sum = 0;
       foreach ($Ventes as $item) {
            $Article = Article::where('ARTID',$item->ARTID)->first();
           if($Article){
                $vente = new Commande;
                $vente->article_id = $Article->id;
                $vente->famille_id = $Article->famille_id;
                $vente->quantite = round($item->PLVQTE);
                $vente->date_vente = date("Y-m-d", strtotime($item->PLVDATE));
                $vente->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Commande';
       $log->description = $sum." Commande(s) Ajoutée(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Vente(s) Ajoutée(s) à la base de donnée.";
    } 

     public function TransfertCommandePlanifieDaily()
    {
       $Ventes = $this->ListeVentesplanifieDaily();
       $sum = 0;
       foreach ($Ventes as $item) {
            $Article = Article::where('ARTID',$item->ARTID)->first();
           if($Article){
                $zonid = 1;
                $vente = new CmdPlan;
                $vente->article_id = $Article->id;
                $vente->famille_id = $Article->famille_id;
                $vente->zone_id = $zonid;
                $vente->quantite = round($item->PLVQTE);
                $vente->date_vente =  date("Y-m-d", strtotime($item->PLVDATE));
                $vente->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Commande Planifiée';
       $log->description = $sum." Commande(s) Planifiée(s) Ajoutée(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Vente(s) Planifié Ajoutée(s) à la base de donnée.";
    } 

     public function TransfertAchatsPlanifieDaily()
    {
       $Achats = $this->ListeAchatsplanifieDaily();
       $sum = 0;
       foreach ($Achats as $item) {
            $Article = Article::where('ARTID',$item->ARTID)->first();
           if($Article){
                $zonid = 1;
                $d = new DateTime($item->PLADATE);
                $d->modify('last day of second month');
                $achat = new Achatplan;
                $achat->article_id = $Article->id;
                $achat->famille_id = $Article->famille_id;
                $achat->quantite = round($item->PLAQTE);
                $achat->date_achat = date('Y-m-d', strtotime($item->PLADATE));
                $achat->date_reception = $d->format('Y-m-d');
                $achat->save();
                $sum = $sum + 1;
           }
       }
       $log = new Log;
       $log->task = 'Ajout Achat Planifiée';
       $log->description = $sum." Achat(s) Planifiée(s) Ajoutée(s) à la base de donnée.";
       $log->date = new DateTime('NOW');
       $log->save();
       return $sum." Vente(s) Planifié Ajoutée(s) à la base de donnée.";
    } 


}
