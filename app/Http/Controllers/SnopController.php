<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Famille;
use App\Models\Commande;
use App\Models\Stock;
use App\Models\Achatplan;
use App\Models\CmdPlan;
use App\Models\Prevision;
use App\Models\Taux;
use App\Models\Article;
use App\Models\Alpha;
use App\Models\Confirmation;
use App\Models\Previsionlog;
use DB;

use Illuminate\Http\Request;

class SnopController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

   
    public function getStock(Request $request)
    {
        $stock = Stock::when($request->famille, function($q) use ($request){    //Search by Nom Article
                                $q->where('famille_id',$request->famille);
                    })
                ->selectRaw(DB::raw('sum(quantite) as Stock'))
                ->where('date',$request->dateDebutStock)
                ->get();
        return $stock;
    }
    
    public function getPrevisions(Request $request)
    {
        // $previsions = Famille::when($request->famille, function($q) use ($request){    //Search by Nom Article
        //             $q->where('id',$request->famille);
        //         })
        //     ->with(['previsions' => function ($q) use($request) {
        //         $q->selectRaw("famille_id,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as YEAR,sum(previsions.prevision) as previsions,prix_moy.prix,SUM(previsions.prevision*prix_moy.prix) AS prixm FROM previsions INNER JOIN prix_moy ON previsions.article_id = prix_moy.article_id WHERE previsions.date BETWEEN ? AND ? GROUP BY famille_id,extract(MONTH from date),extract(year from date) order by Year asc,Mois asc",[$request->dateDebut, $request->dateFin]);
               
               
        //         // ->whereBetween('date', [$request->dateDebut, $request->dateFin])
        //         // ->groupByRaw('famille_id,extract(MONTH from date),extract(YEAR FROM date)')
        //         // ->orderBy('Year','asc')
        //         // ->orderBy('Mois','asc');
        //         }])
        //     ->with(['commandesplani'=>function ($q) use($request) {
        //         $q->selectRaw(DB::raw('famille_id,sum(quantite) as cmdplanifie,extract(MONTH from date_plan) AS Mois,Extract(YEAR FROM date_plan) as Year'))
        //         ->whereBetween('date_plan', [$request->dateDebut, $request->dateFin])
        //         ->groupByRaw('famille_id,extract(MONTH from date_plan),extract(YEAR FROM date_plan)')
        //         ->orderBy('Year','asc')
        //         ->orderBy('Mois','asc');
        //         }])
        //     ->with(['orderreceipt'=>function ($q) use($request) {
        //         $q->selectRaw(DB::raw('famille_id,month,month_rec,sum(po_release) as orders,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
        //         ->whereBetween('date', [$request->dateDebut, $request->dateFin])
        //         ->groupByRaw('famille_id,extract(MONTH from date),extract(YEAR FROM date)')
        //         ->orderBy('Year','asc')
        //         ->orderBy('Mois','asc');
        //     }])
        //     ->with(['achatsplan'=>function ($q) use($request) {
        //         $q->selectRaw(DB::raw('famille_id,sum(quantite) as orders,extract(MONTH from date_reception) AS Mois,Extract(YEAR FROM date_reception) as Year'))
        //         ->whereBetween('date_reception', [$request->dateDebut, $request->dateFin])
        //         ->groupByRaw('famille_id,extract(MONTH from date_reception),extract(YEAR FROM date_reception)')
        //         ->orderBy('Year','asc')
        //         ->orderBy('Mois','asc');
        //     }])
          
        //     ->with(['commandeC'=> function($q) use($request){
        //         $q->selectRaw(DB::raw('famille_id'))
        //         ->selectRaw(DB::raw('sum(price) as Sumventes,sum(quantite) as qte,(sum(price)/sum(quantite)) as PrixMoy'))
        //         ->whereBetween('date_vente', [$request->dateDebutVente, $request->dateFinVente])
        //         ->groupByRaw('famille_id');
        //     }])
        //     ->get();

            $previsions = Famille::when($request->famille, function ($q) use ($request) {
                                $q->where('id',$request->famille);
                            })
                            ->with(['previsions' => function ($q) use($request) {
                                $q->join('prix_moy', function ($join) {
                                    $join->on('previsions.article_id', '=', 'prix_moy.article_id');
                                })
                                ->selectRaw("famille_id,date,extract(MONTH from previsions.date) AS Mois,Extract(YEAR FROM previsions.date) as Year,sum(previsions.prevision) as previsions,prix_moy.prix,SUM(previsions.prevision*prix_moy.prix) AS prixm ")
                                ->whereBetween('previsions.date', [$request->dateDebut, $request->dateFin])
                                ->groupBy('previsions.famille_id','Mois','Year')
                                ->orderBy('Year','asc')
                                ->orderBy('Mois','asc');
                            }])
                            ->with(['commandesplani'=>function ($q) use($request) {
                                $q->join('prix_moy', function ($join) {
                                    $join->on('commandes_planifie.article_id', '=', 'prix_moy.article_id');
                                })
                                ->selectRaw("famille_id,date_plan,sum(quantite) as cmdplanifie,extract(MONTH from date_plan) AS Mois,Extract(YEAR FROM date_plan) as Year,prix_moy.prix,SUM(commandes_planifie.quantite*prix_moy.prix) AS prixm ")
                                ->whereBetween('date_plan', [$request->dateDebut, $request->dateFin])
                                ->groupBy('famille_id','Mois','Year')
                                ->orderBy('Year','asc')
                                ->orderBy('Mois','asc');
                                }])
                            ->with(['orderreceipt'=>function ($q) use($request) {
                                $q->join('prix_moy', function ($join) {
                                    $join->on('confirmation.article_id', '=', 'prix_moy.article_id');
                                })
                                ->selectRaw("famille_id,month,month_rec,sum(po_release) as orders,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year,prix_moy.prix,SUM(confirmation.po_release*prix_moy.prix) AS prixm ")
                                ->whereBetween('date', [$request->dateDebut, $request->dateFin])
                                ->groupBy('famille_id','Mois','Year')
                                ->orderBy('Year','asc')
                                ->orderBy('Mois','asc');
                                }])
                            ->with(['achatsplan'=>function ($q) use($request) {
                                $q->join('prix_moy', function ($join) {
                                    $join->on('achats_planifie.article_id', '=', 'prix_moy.article_id');
                                })
                                ->selectRaw("famille_id,sum(quantite) as orders,extract(MONTH from date_reception) AS Mois,Extract(YEAR FROM date_reception) as Year,prix_moy.prix,SUM(achats_planifie.quantite*prix_moy.prix) AS prixm ")
                                ->whereBetween('date_reception', [$request->dateDebut, $request->dateFin])
                                ->groupBy('famille_id','Mois','Year')
                                ->orderBy('Year','asc')
                                ->orderBy('Mois','asc');
                                }])
                            // ->with(['commandeC'=>function ($q) use($request) {
                            //     $q->join('prix_moy', function ($join) {
                            //         $join->on('achats_planifie.article_id', '=', 'prix_moy.article_id');
                            //     })
                            //     ->selectRaw("famille_id,sum(quantite) as orders,extract(MONTH from date_reception) AS Mois,Extract(YEAR FROM date_reception) as Year'),prix_moy.prix,SUM(achats_planifie*prix_moy.prix) AS prixm ")
                            //     ->whereBetween('date_reception', [$request->dateDebut, $request->dateFin])
                            //     ->groupBy('famille_id','Mois','Year')
                            //     ->orderBy('Year','asc')
                            //     ->orderBy('Mois','asc');
                            //     }])
                        ->get();
            
                // ->with([''=> function($q) use($request){
                //     $q->selectRaw(DB::raw('famille_id'))
                //     ->selectRaw(DB::raw('sum(price) as Sumventes,sum(quantite) as qte,(sum(price)/sum(quantite)) as PrixMoy'))
                //     ->whereBetween('date_vente', [$request->dateDebutVente, $request->dateFinVente])
                //     ->groupByRaw('famille_id');
                // }])
        
        // DB::select('select famille_id,date,sum(quantite) as qte,extract(month from date) as Mois,extract(year from date) as year from stocks where date in (select max(date) from stocks group by extract(month from date)) group by date,famille_id');
        $stocks = DB::select('select date,famille_id,extract(month from date) as Mois,extract(year from date) as Year,sum(quantite) as qte, prix_moy.prix,SUM((quantite*prix_moy.prix)) AS prixm from stocks INNER JOIN prix_moy on stocks.article_id = prix_moy.article_id where date in (select max(date) from stocks group by extract(month from DATE)) group by date,famille_id');
         return [
           'previsions'=>$previsions,
           'stocks'=>$stocks  
         ];
    }

    public function getPrevisionsGlobal(Request $request)
    {
        $previsions = Prevision::join('prix_moy', function ($join) {
                            $join->on('previsions.article_id', '=', 'prix_moy.article_id');
                        })
                        ->selectRaw("date,extract(MONTH from previsions.date) AS Mois,Extract(YEAR FROM previsions.date) as Year,sum(previsions.prevision) as previsions,prix_moy.prix,SUM(previsions.prevision*prix_moy.prix) AS prixm ")
                        ->whereBetween('previsions.date', [$request->dateDebut, $request->dateFin])
                        ->groupBy('Mois','Year')
                        ->orderBy('Year','asc')
                        ->orderBy('Mois','asc')
                        ->get();
        $commandesPlan = CmdPlan::join('prix_moy', function ($join) {
                            $join->on('commandes_planifie.article_id', '=', 'prix_moy.article_id');
                        })
                        ->selectRaw("date_plan,sum(quantite) as cmdplanifie,extract(MONTH from date_plan) AS Mois,Extract(YEAR FROM date_plan) as Year,prix_moy.prix,SUM(commandes_planifie.quantite*prix_moy.prix) AS prixm ")
                        ->whereBetween('date_plan', [$request->dateDebut, $request->dateFin])
                        ->groupBy('Mois','Year')
                        ->orderBy('Year','asc')
                        ->orderBy('Mois','asc')
                        ->get();
        $achatPlan = Achatplan::join('prix_moy', function ($join) {
                            $join->on('achats_planifie.article_id', '=', 'prix_moy.article_id');
                        })
                        ->selectRaw("sum(quantite) as orders,extract(MONTH from date_reception) AS Mois,Extract(YEAR FROM date_reception) as Year,prix_moy.prix,SUM(achats_planifie.quantite*prix_moy.prix) AS prixm ")
                        ->whereBetween('date_reception', [$request->dateDebut, $request->dateFin])
                        ->groupBy('Mois','Year')
                        ->orderBy('Year','asc')
                        ->orderBy('Mois','asc')
                        ->get();
                     
        $orderreceipt = Confirmation::join('prix_moy', function ($join) {
                            $join->on('confirmation.article_id', '=', 'prix_moy.article_id');
                        })
                        ->selectRaw("month,month_rec,sum(po_release) as orders,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year,prix_moy.prix,SUM(confirmation.po_release*prix_moy.prix) AS prixm")
                        ->whereBetween('date', [$request->dateDebut, $request->dateFin])
                        ->groupBy('Mois','Year')
                        ->orderBy('Year','asc')
                        ->orderBy('Mois','asc')
                        ->get();
              
        $stocks = DB::select('select date,extract(month from date) as Mois,extract(year from date) as Year,sum(quantite) as qte, prix_moy.prix,SUM((quantite*prix_moy.prix)) AS prixm from stocks INNER JOIN prix_moy on stocks.article_id = prix_moy.article_id where date in (select max(date) from stocks group by extract(month from DATE)) group by date');
         return [
           'previsions'=>[
               'previsions'=>$previsions,
               'commandesplani'=>$commandesPlan,
               'achatsplan'=>$achatPlan,
               'orderreceipt'=>$orderreceipt,
           ],
           'stocks'=>$stocks  
         ];
    }

    public function getDetailSnop(Request $request)
    {
        $previsions = Famille::where('id',$request->famille)
        ->with(['previsions' => function ($q) use($request) {
            $q->join('prix_moy', function ($join) {
                $join->on('previsions.article_id', '=', 'prix_moy.article_id');
            })
            ->selectRaw("famille_id,date,extract(MONTH from previsions.date) AS Mois,Extract(YEAR FROM previsions.date) as Year,sum(previsions.prevision) as previsions,prix_moy.prix,SUM(previsions.prevision*prix_moy.prix) AS prixm ")
            ->whereBetween('previsions.date', [$request->dateDebut, $request->dateFin])
            ->groupBy('previsions.famille_id','Mois','Year')
            ->orderBy('Year','asc')
            ->orderBy('Mois','asc');
        }])
        ->with(['commandesplani'=>function ($q) use($request) {
            $q->join('prix_moy', function ($join) {
                $join->on('commandes_planifie.article_id', '=', 'prix_moy.article_id');
            })
            ->selectRaw("famille_id,date_plan,sum(quantite) as cmdplanifie,extract(MONTH from date_plan) AS Mois,Extract(YEAR FROM date_plan) as Year,prix_moy.prix,SUM(commandes_planifie.quantite*prix_moy.prix) AS prixm ")
            ->whereBetween('date_plan', [$request->dateDebut, $request->dateFin])
            ->groupBy('famille_id','Mois','Year')
            ->orderBy('Year','asc')
            ->orderBy('Mois','asc');
            }])
        ->with(['orderreceipt'=>function ($q) use($request) {
            $q->join('prix_moy', function ($join) {
                $join->on('confirmation.article_id', '=', 'prix_moy.article_id');
            })
            ->selectRaw("famille_id,month,month_rec,sum(po_release) as orders,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year,prix_moy.prix,SUM(confirmation.po_release*prix_moy.prix) AS prixm ")
            ->whereBetween('date', [$request->dateDebut, $request->dateFin])
            ->groupBy('famille_id','Mois','Year')
            ->orderBy('Year','asc')
            ->orderBy('Mois','asc');
            }])
        ->with(['achatsplan'=>function ($q) use($request) {
            $q->join('prix_moy', function ($join) {
                $join->on('achats_planifie.article_id', '=', 'prix_moy.article_id');
            })
            ->selectRaw("famille_id,sum(quantite) as orders,extract(MONTH from date_reception) AS Mois,Extract(YEAR FROM date_reception) as Year,prix_moy.prix,SUM(achats_planifie.quantite*prix_moy.prix) AS prixm ")
            ->whereBetween('date_reception', [$request->dateDebut, $request->dateFin])
            ->groupBy('famille_id','Mois','Year')
            ->orderBy('Year','asc')
            ->orderBy('Mois','asc');
            }])
       
        ->with(['achats'=>function ($q) use($request) {
            $q->join('prix_moy', function ($join) {
                $join->on('achats_planifie.article_id', '=', 'prix_moy.article_id');
            })
            ->selectRaw("famille_id,sum(quantite) as orders,extract(MONTH from date_reception) AS Mois,Extract(YEAR FROM date_reception) as Year,prix_moy.prix,SUM(achats_planifie.quantite*prix_moy.prix) AS prixm ")
            ->whereBetween('date_reception', [$request->dateDebut, $request->dateFin])
            ->groupBy('famille_id','Mois','Year')
            ->orderBy('Year','asc')
            ->orderBy('Mois','asc');
            }])
       

        ->with(['commandesnop'=>function ($q) use($request) {
            $q->join('prix_moy', function ($join) {
                $join->on('commandes.article_id', '=', 'prix_moy.article_id');
            })
            ->selectRaw("famille_id,sum(quantite) as ventes,extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year,prix_moy.prix,SUM(commandes.quantite*prix_moy.prix) AS prixm ")
            ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
            ->groupBy('famille_id','Mois','Year')
            ->orderBy('Year','asc')
            ->orderBy('Mois','asc');
            
            }])
            ->with(['stocks'=>function ($q) use($request) {
                $q->selectRaw(DB::raw('famille_id,article_id,quantite,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
                ->whereBetween('date', [$request->dateDebutStock, $request->dateFin])
                ->groupByRaw('article_id,date')
                ->orderBy('Year','asc')
                ->orderBy('Mois','asc');
            }])
    
        // ->with(['stocks'=>function ($q) use($request) {
        //     $q->join('prix_moy', function ($join) {
        //         $join->on('stocks.article_id', '=', 'prix_moy.article_id');
        //     })
        //     ->selectRaw('famille_id,quantite,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year,prix_moy.prix,SUM((quantite*prix_moy.prix)) AS prixm from stocks INNER JOIN prix_moy on stocks.article_id = prix_moy.article_id')
        //     ->whereIn('date', DB::select('select max(date) from stocks group by extract(month from DATE)'))
        //     ->whereBetween('date', [$request->dateDebutStock, $request->dateFin])
        //     ->groupByRaw('famille_id,date')
        //     ->orderBy('Year','asc')
        //     ->orderBy('Mois','asc');;
        // }])
        
        ->get();

        $stocks = DB::select('select date,famille_id,extract(month from date) as Mois,extract(year from date) as Year,sum(quantite) as qte, prix_moy.prix,SUM((quantite*prix_moy.prix)) AS prixm from stocks INNER JOIN prix_moy on stocks.article_id = prix_moy.article_id where date in (select max(date) from stocks group by extract(month from DATE)) and famille_id = ? group by date,famille_id',[$request->famille]);
     return [
         'previsions'=>$previsions,
         'stock'=>$stocks
     ];
    }

    public function getCmds(Request $request)
    {
        $cmds = Commande::selectRaw(DB::raw('famille_id,sum(quantite) as ventes,extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year'))
        ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
        ->where('famille_id','1')
        ->groupByRaw('famille_id,extract(MONTH from date_vente),extract(YEAR FROM date_vente)')
        ->orderBy('Year','asc')
        ->orderBy('Mois','asc')
        ->get();
        return $cmds;
    }

    public function testdate()
    {
        $a_date = "2009-11-23";
        return date("Y-m-t", strtotime($a_date));
    }


    public function articleListe(Request $request,$familleid)
    {
        $articles = Article::where('famille_id',$familleid)
        ->with(['previsions'=> function ($q) use($request) {
            $q->selectRaw(DB::raw('id,famille_id,article_id,prevision,date,prevision_old'))
            ->whereBetween('date', [$request->dateDebut, $request->dateFin]);
            }])
        ->with('previsionlog')
        ->paginate(10);
        return $articles;
    }

    public function articleEdit(Request $request)
    {
        
        try{
            $l = Previsionlog::where('prevision_id',$request->prevision_id)->where('date',$request->date)->first();
            if($l){
                $l->cause = $request->cause;
                $l->valeur = $request->newprevision;
                $l->save();
            }else{
                $log = new Previsionlog;
                $log->famille_id = $request->famille_id;
                $log->article_id = $request->article_id;
                $log->prevision_id = $request->prevision_id;
                $log->cause = $request->cause;
                $log->date = $request->date;
                $log->valeur = $request->newprevision;
                $log->save();
            }
           

            $prevision = Prevision::where('id',$request->prevision_id)->first();
            $prevision->prevision = $request->newprevision;
            $prevision->prevision_old = $request->oldprevision;
            $prevision->save();

            return $prevision;
        }catch (\Throwable $th) {
            return ['data'=>$th,'errors'=>null];
        }
    }

    public function testStock()
    {
        $stocks = DB::select('select date,sum(quantite) as qte from stocks where date in (select max(date) from stocks group by extract(month from date)) group by date');
        return $stocks;
    }

}
