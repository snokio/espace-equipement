<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Prix;
use Illuminate\Http\Request;

class PrixController extends Controller
{

    public function getAll()
    {
        $prix = Prix::with('article')->get();
        return $prix;
    }

}
