<?php

namespace App\Http\Controllers;

use App\Models\Achatplan;
use App\Models\Biais;
use App\Models\Famille;
use App\Models\Objectif;
use App\Models\Commande;
use App\Models\Prevision;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;


class BiController extends Controller
{

    public function globalData(Request $request)
    {
        $objectifs = Objectif::selectRaw(DB::raw('extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
                     ->selectRaw(DB::raw('sum(objectif) as objectifSum'))
                     ->whereBetween('date',[$request->dateDebut,$request->dateFin])
                     ->groupByRaw('extract(MONTH from date)')
                     ->get();
        $cmds = Commande::selectRaw(DB::raw('extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year'))
                    ->selectRaw(DB::raw('sum(price) as Sumventes'))
                    ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                    ->groupByRaw('extract(MONTH from date_vente)')
                    ->get();
        $previsions = Prevision::selectRaw(DB::raw('extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
                    ->selectRaw(DB::raw('sum(prevision) as prevision'))
                    ->whereBetween('date', [$request->dateDebut, $request->dateFin])
                    ->groupByRaw('extract(MONTH from date)')
                    ->get();
        return [
            'objectifs'=>$objectifs,
            'commandes'=>$cmds,
            'previsions'=>$previsions
        ];
    }

    public function statsData(Request $request)
    {
        $dateMonth= date("m");
        $dateYear= date("Y");
        $datePrecMonth = date("m");
        $datePrecYear = date("Y");
        $dateSuivMonth = date("m");
        $dateSuivYear = date("Y");
        if($dateMonth == 1){
            $datePrecMonth = 12;
            $datePrecYear = $dateYear-1;
        }else{
            $datePrecMonth = $dateMonth-1;
        }
        if($dateMonth == 12){
            $dateSuivMonth = 1;
            $dateSuivYear = $dateYear+1;
        }else{
            $dateSuivMonth = $dateMonth+1;
        }
        $ventesThisMonth = Commande::selectRaw(DB::raw('extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year'))
                            ->selectRaw(DB::raw('sum(price) as Sumventes,sum(quantite) as quantites'))
                            ->whereMonth('date_vente',$dateMonth)
                            ->whereYear('date_vente',$dateYear)
                            ->get();
        $ventesLastMonth = Commande::selectRaw(DB::raw('extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year'))
                            ->selectRaw(DB::raw('sum(price) as Sumventes,sum(quantite) as quantites'))
                            ->whereMonth('date_vente',$datePrecMonth)
                            ->whereYear('date_vente',$datePrecYear)
                            ->get();
        $objectifThisMonth =  Objectif::selectRaw(DB::raw('extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
                            ->selectRaw(DB::raw('sum(objectif) as objectifSum'))
                            ->whereMonth('date',$dateMonth)
                            ->whereYear('date',$dateYear)
                            ->get();
        $objectifLastMonth =  Objectif::selectRaw(DB::raw('extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
                            ->selectRaw(DB::raw('sum(objectif) as objectifSum'))
                            ->whereMonth('date',$datePrecMonth)
                            ->whereYear('date',$datePrecYear)
                            ->get();
        $stocks = DB::select('select date,extract(month from date) as Mois,extract(year from date) as Year,sum(quantite) as qte from stocks where date in (select max(date) from stocks group by extract(month from date)) group by date');
        
        $stocksP = DB::select('select stocks.date,extract(month from date) as Mois,extract(year from date) as Year,stocks.famille_id,sum(stocks.quantite) as qte, prix_moy.prix,SUM((quantite*prix_moy.prix)) AS prixm
        from stocks INNER JOIN prix_moy on stocks.article_id = prix_moy.article_id where stocks.date in (select max(date) from stocks group by extract(month from date)) group by stocks.date');
        
        $achatsNextMonth = Achatplan::selectRaw(DB::raw('extract(MONTH from date_reception) AS Mois,Extract(YEAR FROM date_reception) as Year'))
                            ->selectRaw(DB::raw('sum(quantite) as qte,count(id) as nbre'))
                            ->whereMonth('date_reception',$dateSuivMonth)
                            ->whereYear('date_reception',$dateSuivYear)
                            ->get();                
        $achatsThisMonth = Achatplan::selectRaw(DB::raw('extract(MONTH from date_reception) AS Mois,Extract(YEAR FROM date_reception) as Year'))
                            ->selectRaw(DB::raw('sum(quantite) as qte,count(id) as nbre'))
                            ->whereMonth('date_reception',$dateMonth)
                            ->whereYear('date_reception',$dateYear)
                            ->get();            
        $prixSume =  Commande::selectRaw(DB::raw('sum(price) as Sumventes,sum(quantite) as qte,(sum(price)/sum(quantite)) as PrixMoy'))
                            ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                            ->get();
        $prixSumc =  Commande::selectRaw(DB::raw('sum(price) as Sumventes,sum(quantite) as qte,(sum(price)/sum(quantite)) as PrixMoy'))
                            ->whereBetween('date_vente', [$request->dateDebutc, $request->dateFinc])
                            ->get();    

        return [
            'ventesEncours'=>$ventesThisMonth,
            'ventesDernier'=>$ventesLastMonth,
            'objectifEncours'=>$objectifThisMonth,
            'objectifDernier'=>$objectifLastMonth,
            'stocks'=>$stocks,
            'stocksM'=>$stocksP,
            'achatsSuivant'=>$achatsNextMonth,
            'achatsEncours'=>$achatsThisMonth,
            'prixMoyc'=>$prixSumc,
            'prixMoye'=>$prixSume

        ];

    }

    public function getTableData(Request $request)
    {
        $objectifs = Objectif::selectRaw(DB::raw('id,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
                     ->selectRaw(DB::raw('sum(objectif) as objectifSum'))
                     ->whereBetween('date',[$request->dateDebut,$request->dateFin])
                     ->groupByRaw('extract(MONTH from date)')
                     ->with(['commandes' => function ($q) use ($request) {
                        $q->selectRaw(DB::raw('famille_id,extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year'))
                        ->selectRaw(DB::raw('sum(price) as Sumventes'))
                        ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                        ->groupByRaw('extract(MONTH from date_vente)');
                    }])
                    ->get();
        $cmds = Commande::selectRaw(DB::raw('extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year'))
                    ->selectRaw(DB::raw('sum(price) as Sumventes'))
                    ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                    ->groupByRaw('extract(MONTH from date_vente)')
                    ->get();
        $previsions = Prevision::selectRaw(DB::raw('extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
                    ->selectRaw(DB::raw('sum(prevision) as prevision'))
                    ->whereBetween('date', [$request->dateDebut, $request->dateFin])
                    ->groupByRaw('extract(MONTH from date)')
                    ->get();
        $previsionsM = DB::select("SELECT extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as YEAR,sum(prevision) as prevision,prix_moy.prix,SUM((prevision*prix_moy.prix)) AS prixm FROM previsions INNER JOIN prix_moy ON previsions.article_id = prix_moy.article_id
                    WHERE date BETWEEN ? AND ? GROUP BY extract(MONTH from date)",[$request->dateDebut, $request->dateFin]);
        $achats = Achatplan::selectRaw(DB::raw('extract(MONTH from date_reception) AS Mois,Extract(YEAR FROM date_reception) as Year'))
                    ->selectRaw(DB::raw('sum(quantite) as qte,count(id) as nbre'))
                    ->whereBetween('date_reception', [$request->dateDebut, $request->dateFin])
                    ->groupByRaw('extract(MONTH from date_reception)')
                    ->get();
        $achatsM = DB::select("SELECT extract(MONTH from date_reception) AS Mois,Extract(YEAR FROM date_reception) as YEAR,sum(quantite) as qte,prix_moy.prix,SUM((quantite*prix_moy.prix)) AS prixm FROM achats_planifie INNER JOIN prix_moy ON achats_planifie.article_id = prix_moy.article_id
        WHERE DATE_reception BETWEEN ? AND ? GROUP BY extract(MONTH from date_reception)",[$request->dateDebut, $request->dateFin]);
        $prixSume =  Commande::selectRaw(DB::raw('sum(price) as Sumventes,sum(quantite) as qte,(sum(price)/sum(quantite)) as PrixMoy'))
                ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                ->get();
        $prixSumc =  Commande::selectRaw(DB::raw('sum(price) as Sumventes,sum(quantite) as qte,(sum(price)/sum(quantite)) as PrixMoy'))
                ->whereBetween('date_vente', [$request->dateDebutc, $request->dateFinc])
                ->get();
        $stocks = DB::select('select date,extract(month from date) as Mois,extract(year from date) as Year,sum(quantite) as qte from stocks where date in (select max(date) from stocks group by extract(month from date)) and date between ? and ? group by date', [$request->dateDebut, $request->dateFin]);   
       
        $stockM = DB::select('select date,extract(month from date) as Mois,extract(year from date) as Year,sum(quantite) as qte, prix_moy.prix,SUM((quantite*prix_moy.prix)) AS prixm from stocks INNER JOIN prix_moy on stocks.article_id = prix_moy.article_id where date in (select max(date) from stocks group by extract(month from DATE)) and date BETWEEN ? AND ? group by date', [$request->dateDebut, $request->dateFin]);   
        return [
            'objectifs'=>$objectifs,
            'commandes'=>$cmds,
            'previsions'=>$previsions,
            'previsionsM'=>$previsionsM,
            'achats'=>$achats,
            'achatsM'=>$achatsM,
            'stocks'=>$stocks,
            'stocksM'=>$stockM,
            'prixe'=>$prixSume,
            'prixc'=>$prixSumc,
        ];
    }

    public function getTableDataDetail(Request $request)
    {
       $pDateD = date("Y-m-01", strtotime("-6 months"));
       $pDateF = date("Y-m-31", strtotime("-1 months"));
       $familles = Famille::all();
       $objectifs = Objectif::where('date',$request->dateObjectif)
                ->with('famille')
                ->get();
       $cmds = Commande::join('familles','commandes.famille_id','=','familles.id')
                ->selectRaw(DB::raw('famille_id,familles.famille,extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year'))
                ->selectRaw(DB::raw('sum(price) as Sumventes'))
                ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                ->groupByRaw('famille_id,extract(MONTH from date_vente)')
                ->get();
       $previsions = Prevision::join('familles','previsions.famille_id','=','familles.id')
                ->selectRaw(DB::raw('famille_id,familles.famille,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
                ->selectRaw(DB::raw('sum(prevision) as prevision'))
                ->whereBetween('date', [$request->dateDebut, $request->dateFin])
                ->groupByRaw('famille_id,extract(MONTH from date)')
                ->get();
        $previsionsM = DB::select("SELECT extract(MONTH from date) AS Mois,famille_id,Extract(YEAR FROM date) as YEAR,sum(prevision) as prevision,prix_moy.prix,SUM((prevision*prix_moy.prix)) AS prixm FROM previsions INNER JOIN prix_moy ON previsions.article_id = prix_moy.article_id
                WHERE date BETWEEN ? AND ? GROUP BY extract(MONTH from date),famille_id",[$request->dateDebut, $request->dateFin]);
       $mix = Commande::join('familles','commandes.famille_id','=','familles.id')
                ->selectRaw(DB::raw('famille_id,familles.famille'))
                ->selectRaw(DB::raw('sum(price) as Sumventes'))
                ->whereBetween('date_vente', [$request->dateDebutMix, $request->dateFinMix])
                ->groupByRaw('famille_id')
                ->get();
        $mixTotal =  Commande::selectRaw(DB::raw('sum(price) as Sumventes'))
                ->whereBetween('date_vente', [$request->dateDebutMix, $request->dateFinMix])
                ->get();
        $articles = Commande::join('articles','commandes.article_id','=','articles.id')
                ->selectRaw(DB::raw('article_id,articles.code'))
                ->selectRaw(DB::raw('sum(price) as Sumventes,sum(quantite) as qte,(sum(price)/sum(quantite)) as PrixMoy'))
                ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                ->groupByRaw('article_id')
                ->get();
        $stocks = DB::select('select date,famille_id,extract(month from date) as Mois,extract(year from date) as Year,sum(quantite) as qte from stocks where date in (select max(date) from stocks group by extract(month from date)) and date between ? and ? group by date,famille_id', [$request->dateDebut, $request->dateFin]);   
        $stocksM = DB::select('select date,famille_id,extract(month from date) as Mois,extract(year from date) as Year,sum(quantite) as qte, prix_moy.prix,SUM((quantite*prix_moy.prix)) AS prixm from stocks INNER JOIN prix_moy on stocks.article_id = prix_moy.article_id where date in (select max(date) from stocks group by extract(month from DATE)) and date BETWEEN ? AND ? group by date,famille_id', [$request->dateDebut, $request->dateFin]);   
        $achats = Achatplan::selectRaw(DB::raw('famille_id,extract(MONTH from date_reception) AS Mois,Extract(YEAR FROM date_reception) as Year'))
                    ->selectRaw(DB::raw('sum(quantite) as qte,count(id) as nbre'))
                    ->whereBetween('date_reception', [$request->dateDebut, $request->dateFin])
                    ->groupByRaw('extract(MONTH from date_reception),famille_id')
                    ->get();
        $achatsM = DB::select("SELECT extract(MONTH from date_reception) AS Mois,famille_id,Extract(YEAR FROM date_reception) as YEAR,sum(quantite) as qte,prix_moy.prix,SUM((quantite*prix_moy.prix)) AS prixm FROM achats_planifie INNER JOIN prix_moy ON achats_planifie.article_id = prix_moy.article_id
                    WHERE DATE_reception BETWEEN ? AND ? GROUP BY extract(MONTH from date_reception),famille_id",[$request->dateDebut, $request->dateFin]);
        $prixSum =  Commande::selectRaw(DB::raw('sum(price) as Sumventes,sum(quantite) as qte,(sum(price)/sum(quantite)) as PrixMoy'))
                ->whereBetween('date_vente', [$request->dateDebutc, $request->dateFinc])
                ->get();
        // $prixe =  Commande::selectRaw(DB::raw('famille_id,sum(price) as Sumventes,sum(quantite) as qte,(sum(price)/sum(quantite)) as PrixMoy'))
        //         ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
        //         ->groupByRaw('famille_id')
        //         ->get();
        $prixe =  Commande::selectRaw(DB::raw('famille_id,sum(price) as Sumventes,sum(quantite) as qte,(sum(price)/sum(quantite)) as PrixMoy'))
                ->whereBetween('date_vente', [$pDateD, $pDateF])
                ->groupByRaw('famille_id')
                ->get();
        $prixc =  Commande::selectRaw(DB::raw('famille_id,sum(price) as Sumventes,sum(quantite) as qte,(sum(price)/sum(quantite)) as PrixMoy'))
                ->whereBetween('date_vente', [$request->dateDebutc, $request->dateFinc])
                ->groupByRaw('famille_id')
                ->get();

        return [
        'objectifs'=>$objectifs,
        'commandes'=>$cmds,
        'familles'=>$familles,
        'articles'=>$articles,
        'mix'=>$mix,
        'mixTotal'=>$mixTotal,
        'previsions'=>$previsions,
        'previsionsM'=>$previsionsM,
        'stocks'=>$stocks,
        'stocksM'=>$stocksM,
        'achats'=>$achats,
        'achatsM'=>$achatsM,
        'prixe'=>$prixe,
        'prixc'=>$prixc,
        'prixSum'=>$prixSum
        ];
    }


}
