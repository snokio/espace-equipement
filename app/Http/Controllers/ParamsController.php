<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Params;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

class ParamsController extends Controller
{
       //get all articles
       public function getParams($articleid)
       {
           $params = Params::where('article_id',$articleid)->first();
           if($params){
               return $params;
           }else{
                return $this->getParamsDefault();
           }
       }

       public function saveParams(Request $request)
       {
           $param = Params::where('article_id',$request->article_id)->first();
           if($param){
               $param->lot = $request->lot;
               $param->delai = $request->delai;
               $param->dtf = $request->dtf;
               $param->stock = $request->stock;
               return $param->save();
           }else{
               $param = new Params;
                $param->article_id = $request->article_id;
                $param->lot = $request->lot;
                $param->delai = $request->delai;
                $param->dtf = $request->dtf;
                $param->stock = $request->stock;
                return $param->save();
           }
       }
       
       public function getParamsDefault()
       {
           $d = Params::where('article_id',0)->first();
           return $d;
       }
}
