<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use PHPUnit\Framework\Constraint\Count;

class ArticleController extends Controller
{
       //get all articles
       public function getArticles()
       {
           $articles = Article::with('famille')->paginate(15);
           return $articles;
       }
       public function getAll()
       {
           $articles = Article::all();
           return $articles;
       }

       public function getArticlesFamille($familleID)
       {
           $articles = Article::where('famille_id',$familleID)->get();
           return $articles;
       }
       public function getArticlesralance(Request $request)
       {
           $stocko = Stock::orderBy('id','desc')->first();
           $stocks = DB::select('select date,famille_id,extract(month from date) as Mois,extract(year from date) as Year,sum(quantite) as qte from stocks where date in (select max(date) from stocks group by extract(month from date)) group by date,famille_id'); 
        //    $lastDate = $stocks[Count($stocks)-1]=>;
            $dateStock = '';
            $d = $stocks[count($stocks)-1];
            if($d->Mois == (int) date("m")){
                $dateStock = $stocks[count($stocks)-2]->date;
            }else{
                $dateStock = $d->date;
            }

            $articles = Article::when($request->familleID, function($query) use ($request){                   //Search by Famille
                $query->where('famille_id',$request->familleID);
                })
            ->with(['stocksM' => function ($q) use ($dateStock) {
                     $q->selectRaw(DB::raw('article_id,extract(MONTH from date) AS Mois,Extract(YEAR FROM date) as Year'))
                     ->selectRaw(DB::raw('sum(quantite) as Sumstock'))
                     ->where('date',$dateStock)
                     ->groupByRaw('article_id,extract(MONTH from date)')
                     
                    //  ->latest('date')->first();
                    ->get();
                 }])
             ->with(['commandesM' => function ($q) use ($request) {
                     $q->selectRaw(DB::raw('article_id,extract(MONTH from date_vente) AS Mois,Extract(YEAR FROM date_vente) as Year'))
                     ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
                     ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                     ->groupByRaw('article_id,extract(MONTH from date_vente)')
                     ->orderBy('Year','asc')
                     ->orderBy('Mois','asc');
                 }])
            ->with('famille')
             ->get();
            return $articles;
        
       }

       public function dd()
       {
        $stocks = DB::select('select date,extract(month from date) as Mois,extract(year from date) as Year,sum(quantite) as qte from stocks where date in (select max(date) from stocks group by extract(month from date)) group by date'); 
        $d = $stocks[count($stocks)-1];
        if($d->Mois == (int) date("m")){
            return [
                'data'=>$stocks[count($stocks)-2]->date
            ];
        }else{
            return [
                'data'=>$d->date,
            ];
        }
       
       }

       public function getArticlesWithRelations()
       {
           $articles = Article::with(['famille','commandes','previsions'])
           ->paginate(15);
           return $articles;
       }
   
       //get article info
       public function articleInfo($id)
       {
           try{
               $article = Article::where('id',$id)
               ->with(['famille','commandes','previsions'])
               ->first();
               return $article;
   
           }catch (\Throwable $th) {
               return ['data'=>null,'errors'=>null];
           }
       }

        
   
   
       //create article
       public function createArticle(Request $request)
       {
           try{
               $validator = Validator::make($request->all(), [
                   'code' => 'required',
                   'designation' => 'required',
                   'famille' => 'required',
                   'classe'=>'required'
               ]);
   
               if ($validator->fails()) {
                   return ['data'=>null,'errors'=>$validator->errors()];
               }
               
               $article = new Article;
               $article->code = $request->code;
               $article->designation = $request->designation;
               $article->famille_id = $request->famille;
               $article->classe = $request->classe;
               $article->save();
               return $article;
   
           }catch (\Throwable $th) {
               return ['data'=>$th,'errors'=>null];
           }
       }
       
       //edit article
       public function editArticle(Request $request,$id)
       {
           try{
               $validator = Validator::make($request->all(), [
                    'code' => 'required',
                    'designation' => 'required',
                    'famille' => 'required',
                    'classe'=>'required'
               ]);
   
               if ($validator->fails()) {
                   return ['data'=>null,'errors'=>$validator->errors()];
               }
               
               $article = Article::where('id',$id)->first();
               $article->code = $request->code;
               $article->designation = $request->designation;
               $article->famille_id = $request->famille;
               $article->classe = $request->classe;
               $article->save();
               return $article;
   
           }catch (\Throwable $th) {
               return ['data'=>$th,'errors'=>null];
           }
       }
   
       //delete article
       public function deleteArticle($id)
       {   
           try{
               $article = Article::where('id',$id)->first();
               $article->delete();
               return $article;
   
           }catch (\Throwable $th) {
               return ['data'=>null,'errors'=>null];
           }
       }

       public function stocks(Request $request)
       {
           $stocks = Stock::selectRaw(DB::raw('id,article_id,quantite,extract(MONTH from date) AS Mois,extract(YEAR FROM date) as Year'))
                    ->when(($request->dateDebut && $request->dateFin), function($query) use ($request){                    //Search by Nom Client
                        $query->whereBetween('date', [$request->dateDebut, $request->dateFin]);
                    })
                    ->when($request->code, function($query) use ($request){                    //Search by Nom Client
                            $query->whereHas('articlesecure', function($q) use ($request) {
                                $q->where('code', 'like', '%'.$request->code.'%');
                            });
                    })
                    ->groupByRaw('id,article_id,Extract( MONTH FROM date ),Extract(YEAR from date)')

                   ->orderBy('Year','asc')
                   ->orderBy('mois','asc')
                    ->orderBy('id','desc')
                    ->get();
            
            return $stocks;
       }

       public function changeClasse(Request $request)
       {        
           $article = Article::where('id',$request->id)->first();
           if($article){
                $article->classe = $request->classe;
                $article->save();
                return 1;
           }else{
                 return 0;
           }
           
        }
       public function changeStrategie(Request $request)
       {        
           $article = Article::where('id',$request->id)->first();
           
           if($article){
                $article->strategie = $request->strategie;
                $article->save();
                return 1;
           }else{
               return 0;
           }
        }

}
