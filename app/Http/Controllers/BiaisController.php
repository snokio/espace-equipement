<?php

namespace App\Http\Controllers;

use App\Models\Biais;
use App\Models\Famille;
use App\Models\Article;
use App\Models\Commande;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;


class BiaisController extends Controller
{


    public function biaisFamille(Request $request)
    {

        $Biais = Famille::when($request->famille, function($q) use ($request){    //Search by Nom Article
                 $q->where('id',$request->famille);
             })
        ->with(['commandeG'=>function($q) use ($request){    //Search by Nom Article
             $q->selectRaw(DB::raw('famille_id,extract(MONTH from date_vente) AS Mois,extract(YEAR FROM date_vente) as Year'))
             ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
             ->selectRaw(DB::raw('COUNT(Extract( MONTH FROM date_vente )) AS NombreVente'))
             ->selectRaw(DB::raw('COUNT(DISTINCT Extract( MONTH FROM date_vente )) AS NombreMois'))
             ->selectRaw(DB::raw('avg(quantite) AS MoyenneVente'))
             ->selectRaw(DB::raw('(sum(quantite)/COUNT(DISTINCT Extract( MONTH FROM date_vente ))) AS MoyennePeriode'))
             ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
             ->groupByRaw('famille_id,Extract( MONTH FROM date_vente ),extract(YEAR FROM date_vente)')
             ->orderBy('Year','asc')
        ->orderBy('Mois','asc');
          }])
        ->with(['previsions'=> function($q) use ($request){
              $q->selectRaw(DB::raw('famille_id,Extract( MONTH FROM date ) as Mois,extract(YEAR FROM date) as Year,sum(prevision) as Sumprevision'))
                ->whereBetween('date', [$request->dateDebut, $request->dateFin])
                ->groupByRaw('famille_id,Extract( MONTH FROM date ),extract(YEAR From date)')
                ->orderBy('Year','asc')
                ->orderBy('Mois','asc');
                 }])
        ->groupBy('id')
        ->paginate(100);
        return $Biais;
    }

    public function biais(Request $request)
    {
        $json_response = collect();
        $Biais = Article::select('id','code','designation','classe')
        ->when($request->article, function($q) use ($request){    //Search by Nom Article
                 $q->where('designation', 'like', '%'.$request->article.'%');
             })
         ->when($request->code, function($q) use ($request){                       //Search by Code Article
                 $q->where('code',$request->code);
             })
         ->when($request->famille, function($query) use ($request){                   //Search by Famille
                     $query->whereHas('famille', function($q) use ($request) {
                         $q->where('famille',$request->famille);
                     });
             }) 
         ->when($request->client, function($query) use ($request){                    //Search by Nom Client
                 $query->whereHas('commandes', function($q) use ($request) {
                     $q->whereHas('client', function($qi) use ($request){
                         $qi->where('designation', 'like', '%'.$request->client.'%');
                     });
                 });
             })
         ->when(($request->dateDebut && $request->dateFin), function($query) use ($request){                    //Search by Nom Client
                 $query->whereHas('commandes', function($q) use ($request) {
                     $q->whereBetween('date_vente', [$request->dateDebut, $request->dateFin]);
                 });
             })           
        //  ->with(['commandeG'=> function($q) use ($request){
        //       $q->selectRaw(DB::raw('article_id,extract(MONTH from date_vente) AS Mois'))
        //         ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
        //         ->selectRaw(DB::raw('COUNT(Extract( MONTH FROM date_vente )) AS NombreVente'))
        //         ->selectRaw(DB::raw('COUNT(DISTINCT Extract( MONTH FROM date_vente )) AS NombreMois'))
        //         ->selectRaw(DB::raw('avg(quantite) AS MoyenneVente'))
        //         ->selectRaw(DB::raw('(sum(quantite)/COUNT(DISTINCT Extract( MONTH FROM date_vente ))) AS MoyennePeriode'))
        //         ->selectRaw(DB::raw('(sum(quantite)/COUNT(DISTINCT Extract( MONTH FROM date_vente ))) AS MoyennePeriode'))
        //         ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
        //         ->groupByRaw('article_id');
        //          }])
         ->with(['commandeD'=> function($q) use ($request){
              $q->selectRaw(DB::raw('article_id,extract(MONTH from date_vente) AS Mois,extract(YEAR FROM date_vente) as Year'))
                ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
                // ->selectRaw(DB::raw('COUNT(Extract( MONTH FROM date_vente )) AS NombreVente'))
                // ->selectRaw(DB::raw('COUNT(DISTINCT Extract( MONTH FROM date_vente )) AS NombreMois'))
                // ->selectRaw(DB::raw('avg(quantite) AS MoyenneVente'))
                ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                
                ->whereHas('zone', function($qi) use ($request){
                         $qi->where('zone', 'like', '%'.$request->zone.'%');
                     })
                ->groupByRaw('article_id,Extract( MONTH FROM date_vente ),extract(YEAR FROM date_vente)')
                ->orderBy('Year','asc')
                ->orderBy('Mois','asc');
                 }])
        ->with(['previsions'=> function($q) use ($request){
              $q->selectRaw(DB::raw('article_id,Extract( MONTH FROM date ) as mois,extract(YEAR FROM date) as Year,sum(prevision) as Sumprevision'))
                ->whereBetween('date', [$request->dateDebut, $request->dateFin])
                ->groupByRaw('article_id,Extract( MONTH FROM date ),extract(YEAR FROM date)')
                ->orderBy('Year','asc')
                ->orderBy('Mois','asc');
                 }])
        ->with('famille')
        ->paginate(20000);
        return $Biais;
    }



       //get all biais
       public function getBiais()
       {
           $biais = Biais::paginate(15);
           return $biais;
       }
   
       //get biais info
       public function biaisInfo($id)
       {
           try{
               $biais = Biais::where('id',$id)->first();
               return $biais;
   
           }catch (\Throwable $th) {
               return ['data'=>null,'errors'=>null];
           }
       }
   
       //create biais
       public function createBiais(Request $request)
       {
           try{
               $validator = Validator::make($request->all(), [
                   'famille' => 'required',
                   'article' => 'required',
                   'zone' => 'required',
                   'prevision' => 'required',
                   'cause' => 'required',
                   'action' => 'required'
               ]);
   
               if ($validator->fails()) {
                   return ['data'=>null,'errors'=>$validator->errors()];
               }
               
               $biais = new Biais;
               $biais->famille = $request->famille;
               $biais->article = $request->article;
               $biais->zone = $request->zone;
               $biais->prevision = $request->prevision;
               $biais->cause = $request->designation;
               $biais->action = $request->action;
               $biais->save();
               return $biais;
   
           }catch (\Throwable $th) {
               return ['data'=>null,'errors'=>null];
           }
       }
       
       //edit biais
       public function editBiais(Request $request,$id)
       {
           try{
               $validator = Validator::make($request->all(), [
                    'famille' => 'required',
                    'article' => 'required',
                    'zone' => 'required',
                    'prevision' => 'required',
                    'cause' => 'required',
                    'action' => 'required'
               ]);
   
               if ($validator->fails()) {
                   return ['data'=>null,'errors'=>$validator->errors()];
               }
               
               $biais = Biais::where('id',$id)->first();
               $biais->famille = $request->famille;
               $biais->article = $request->article;
               $biais->zone = $request->zone;
               $biais->prevision = $request->prevision;
               $biais->cause = $request->cause;
               $biais->action = $request->action;
               
               $biais->save();
               return $biais;
   
           }catch (\Throwable $th) {
               return ['data'=>null,'errors'=>null];
           }
       }
   
       //delete biais
       public function deleteBiais($id)
       {   
           try{
               $biais = Biais::where('id',$id)->first();
               $biais->delete();
               return $biais;
   
           }catch (\Throwable $th) {
               return ['data'=>null,'errors'=>null];
           }
       }
}
