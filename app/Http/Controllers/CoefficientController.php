<?php

namespace App\Http\Controllers;

use App\Models\Coefficient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Article;
use DB;

class CoefficientController extends Controller
{
    public function cv(Request $request)
       {
        $json_response = collect();
           $Commandes = Article::when($request->article, function($q) use ($request){    //Search by Nom Article
                    $q->where('designation', 'like', '%'.$request->article.'%');
                })
            ->when($request->code, function($q) use ($request){                       //Search by Code Article
                    $q->where('code',$request->code);
                })
            ->when($request->classe, function($q) use ($request){                       //Search by Code Article
                    $q->where('classe', 'like', '%'.$request->classe.'%');
                })
            ->when($request->famille, function($query) use ($request){                   //Search by Famille
                        $query->whereHas('famille', function($q) use ($request) {
                            $q->where('famille',$request->famille);
                        });
                }) 
            ->when($request->client, function($query) use ($request){                    //Search by Nom Client
                    $query->whereHas('commandes', function($q) use ($request) {
                        $q->whereHas('client', function($qi) use ($request){
                            $qi->where('designation', 'like', '%'.$request->client.'%');
                        });
                    });
                })
            ->when($request->zone, function($query) use ($request){                    //Search by Nom Zone
                    $query->whereHas('commandes', function($q) use ($request) {
                        $q->whereHas('zone', function($qi) use ($request){
                            $qi->where('zone', 'like', '%'.$request->zone.'%');
                        });
                    });
                })
            // ->when(($request->dateDebut && $request->dateFin), function($query) use ($request){                    //Search by Nom Client
            //         $query->whereHas('commandes', function($q) use ($request) {
            //             $q->whereBetween('date_vente', [$request->dateDebut, $request->dateFin]);
            //         });
            //     })           
            // ->with(['commandeG'=> function($q) use ($request){
            //      $q->selectRaw(DB::raw('zone_id,article_id,extract(MONTH from date_vente) AS Mois'))
            //        ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
            //        ->selectRaw(DB::raw('COUNT(Extract( MONTH FROM date_vente )) AS NombreVente'))
            //        ->selectRaw(DB::raw('COUNT(DISTINCT Extract( MONTH FROM date_vente )) AS NombreMois'))
            //        ->selectRaw(DB::raw('avg(quantite) AS MoyenneVente'))
            //        ->selectRaw(DB::raw('(sum(quantite)/COUNT(DISTINCT Extract( MONTH FROM date_vente ))) AS MoyennePeriode'))
            //        ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
            //        ->whereHas('zone', function($qi) use ($request){
            //                 $qi->where('zone', 'like', '%'.$request->zone.'%');
            //             })
            //        ->groupByRaw('article_id');
            //         }])
            ->with(['commandeV'=> function($q) use ($request){
                 $q->selectRaw(DB::raw('article_id'))
                   ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
                   ->selectRaw(DB::raw('COUNT(Extract( MONTH FROM date_vente )) AS NombreVente'))
                   ->selectRaw(DB::raw('COUNT(DISTINCT Extract( MONTH FROM date_vente )) AS NombreMois'))
                   ->selectRaw(DB::raw('avg(quantite) AS MoyenneVente'))
                   ->selectRaw(DB::raw('(sum(quantite)/COUNT(DISTINCT Extract( MONTH FROM date_vente ))) AS MoyennePeriode'))
                   ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                   ->whereHas('zone', function($qi) use ($request){
                            $qi->where('zone', 'like', '%'.$request->zone.'%');
                        })
                   ->groupByRaw('article_id');
                    }])
            ->with(['commandeZone'=> function($q) use ($request){
                        $q->selectRaw(DB::raw('zone_id,article_id,extract(MONTH from date_vente) AS Mois'))
                          ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
                          ->selectRaw(DB::raw('COUNT(Extract( MONTH FROM date_vente )) AS NombreVente'))
                          ->selectRaw(DB::raw('COUNT(DISTINCT Extract( MONTH FROM date_vente )) AS NombreMois'))
                          ->selectRaw(DB::raw('avg(quantite) AS MoyenneVente'))
                          ->selectRaw(DB::raw('(sum(quantite)/COUNT(DISTINCT Extract( MONTH FROM date_vente ))) AS MoyennePeriode'))
                          ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                          ->whereHas('zone', function($qi) use ($request){
                                   $qi->where('zone', 'like', '%'.$request->zone.'%');
                               })
                          ->with('zone')
                          ->groupByRaw('article_id,zone_id');
                    }]) 
            ->with(['commandeZd'=> function($q) use ($request){
                        $q->selectRaw(DB::raw('zone_id,article_id,extract(MONTH from date_vente) AS Mois'))
                          ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
                          ->selectRaw(DB::raw('COUNT(Extract( MONTH FROM date_vente )) AS NombreVente'))
                          ->selectRaw(DB::raw('COUNT(DISTINCT Extract( MONTH FROM date_vente )) AS NombreMois'))
                          ->selectRaw(DB::raw('avg(quantite) AS MoyenneVente'))
                          ->selectRaw(DB::raw('(sum(quantite)/COUNT(DISTINCT Extract( MONTH FROM date_vente ))) AS MoyennePeriode'))
                          ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                          ->whereHas('zone', function($qi) use ($request){
                                   $qi->where('zone', 'like', '%'.$request->zone.'%');
                               })
                          ->groupByRaw('article_id,zone_id,Extract( MONTH FROM date_vente )');
                    }])             
           ->with(['commandeD'=> function($q) use ($request){
                 $q->selectRaw(DB::raw('article_id,extract(MONTH from date_vente) AS Mois'))
                   ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
                   ->selectRaw(DB::raw('COUNT(Extract( MONTH FROM date_vente )) AS NombreVente'))
                   ->selectRaw(DB::raw('COUNT(DISTINCT Extract( MONTH FROM date_vente )) AS NombreMois'))
                   ->selectRaw(DB::raw('avg(quantite) AS MoyenneVente'))
                   ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                   ->whereHas('zone', function($qi) use ($request){
                            $qi->where('zone', 'like', '%'.$request->zone.'%');
                        })
                   ->groupByRaw('article_id,Extract( MONTH FROM date_vente )');
                    }])
           ->with(['commandeC'=> function($q) use ($request){
                 $q->selectRaw(DB::raw('article_id,extract(MONTH from date_vente) AS Mois,extract(YEAR FROM date_vente) as Year'))
                   ->selectRaw(DB::raw('sum(quantite) as Sumventes'))
                   ->selectRaw(DB::raw('COUNT(Extract( MONTH FROM date_vente )) AS NombreVente'))
                   ->selectRaw(DB::raw('COUNT(DISTINCT Extract( MONTH FROM date_vente )) AS NombreMois'))
                   ->selectRaw(DB::raw('avg(quantite) AS MoyenneVente'))
                   ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
                //    ->whereHas('zone', function($qi) use ($request){
                //             $qi->where('zone', 'like', '%'.$request->zone.'%');
                //         })
                   ->groupByRaw('article_id,Extract( MONTH FROM date_vente ),Extract( YEAR FROM date_vente )')
                   ->orderBy('Year','asc')
                   ->orderBy('Mois','asc');
                    }])
            // ->with(['commandes'=> function($q) use ($request){
            //      $q->selectRaw(DB::raw('*'))
            //        ->whereBetween('date_vente', [$request->dateDebut, $request->dateFin])
            //        ->whereHas('zone', function($qi) use ($request){
            //                 $qi->where('zone', 'like', '%'.$request->zone.'%');
            //             })
            //        ->orderBy('zone_id','asc');

            //         }])
            ->with(['previsions'=> function($q) use ($request){
                $q->selectRaw(DB::raw('article_id,Extract( MONTH FROM date ) as mois,sum(prevision) as Sumprevision'))
                    ->whereBetween('date', [$request->dateDebut, $request->dateFin])
                    ->groupByRaw('article_id,Extract( MONTH FROM date )');
            }])
            ->with(['previsionsC'=> function($q) use ($request){
                $q->selectRaw(DB::raw('article_id,Extract( MONTH FROM date ) as mois,Extract(YEAR FROM date) as Year,sum(prevision) as Sumprevision'))
                    ->whereBetween('date', [$request->dateDebut, $request->dateFin])
                    ->groupByRaw('article_id,Extract( MONTH FROM date ),Extract(YEAR FROM date)')
                    ->orderBy('Year','asc')
                    ->orderBy('Mois','asc');
            }])
           ->with('famille')
           ->chunk(100, function ($users) use($json_response) {
                foreach ($users as $user) {
                    $json_response->push($user);
                }
            });

            return $json_response->toJson();
        //    ->paginate(10);
        //    return $Commandes;
       }
}
