<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Objectif extends Model
{
    use HasFactory;
    protected $table = 'objectifs';

    public function famille()
    {
        return $this->belongsTo('App\Models\Famille','famille_id');
    }
    public function commandes()
    {
        return $this->hasMany('App\Models\Commande','famille_id','famille_id');
    }

}
