<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Previsionlog extends Model
{
    use HasFactory;
    protected $table = 'prevision_logs';
    
    public function familles(){
       return $this->hasMany('App\Models\Familles','famille_id');
    }
    
    public function articles(){
       return $this->belongsTo('App\Models\Article','article_id');
    }

    public function prevision(){
        return $this->belongsTo('App\Models\Prevision','prevision_id');
     }
}
