<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Famille extends Model
{
    use HasFactory;
   
    // public function achats(){
    //     return $this->hasMany('App\Models\Achat','famille_id');
    // }
    public function achats(){
        return $this->hasMany('App\Models\Achatplan','famille_id');
    }
    public function achatsplan(){
        return $this->hasMany('App\Models\Achatplan','famille_id');
    }
    
    public function stocks(){
        return $this->hasMany('App\Models\Stock','famille_id');
    }
    public function articles(){
        return $this->hasMany('App\Models\Article','famille_id');
    }

    public function orderreceipt()
    {
        return $this->hasMany('App\Models\Confirmation','famille_id');
    }
    public function commandesplani()
    {
        return $this->hasMany('App\Models\CmdPlan','famille_id');
    }
    public function commandeG()
    {
        return $this->hasMany('App\Models\Commande','famille_id');
    }
    public function commandeC()
    {
        return $this->hasMany('App\Models\Commande','famille_id');
    }

     public function commandeD()
    {
        return $this->hasMany('App\Models\Commande','article_id');
    }

    public function commandes()
    {
        return $this->hasMany('App\Models\Commande','article_id');
    }

    public function commandesnop()
    {
        return $this->hasMany('App\Models\Commande','famille_id');
    }


    public function commandeM()
    {
        return $this->hasMany('App\Models\Commande','famille_id');
    }
    public function commandeZone()
    {
        return $this->hasMany('App\Models\Commande','article_id');
    }
     public function commandeZd()
    {
        return $this->hasMany('App\Models\Commande','article_id');
    }
    public function previsions()
    {
        return $this->hasMany('App\Models\Prevision','famille_id');
    }
    public function previsioncount()
    {
        return $this->hasMany('App\Models\Prevision','famille_id');
    }
    public function previsionsm()
    {
        return $this->hasMany('App\Models\Prevision','famille_id');
    }
    public function previsionsC()
    {
        return $this->hasMany('App\Models\Prevision','famille_id');
    }
    public function commandeone()
    {
        return $this->hasMany('App\Models\Commande','famille_id');
    }
    public function commandetwo()
    {
        return $this->hasMany('App\Models\Commande','famille_id');
    }
    public function commandethree()
    {
        return $this->hasMany('App\Models\Commande','famille_id');
    }

    public function previsions2()
    {
        return $this->hasMany('App\Models\Prevision','famille_id');
    }
    public function stockio()
    {
        return DB::select('select date,sum(quantite) as qte from stocks where date in (select max(date) from stocks group by extract(month from date)) group by date');
    }
}
