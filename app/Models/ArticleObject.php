<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class ArticleObject extends Model
{
    use HasFactory;
    protected $table = 'article_objectifs';

    public function article()
    {
        return $this->belongsTo('App\Models\Article','article_id');
    }
    public function famille()
    {
        return $this->belongsTo('App\Models\Famille','famille_id');
    }
  
     public function commandeZd()
    {
        return $this->hasMany('App\Models\Commande','article_id');
    }

}
