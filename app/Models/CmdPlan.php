<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CmdPlan extends Model
{
    use HasFactory;
    protected $table = 'commandes_planifie';
    
    public function article()
    {
        return $this->belongsTo('App\Models\Article','article_id');
    }

    public function famille()
    {
        return $this->belongsTo('App\Models\Famille','famille_id');
    }

   
}
