<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    use HasFactory;
    protected $table = 'stocks';
 
    public function articles(){
       return $this->hasMany('App\Models\Article','article_id');
    } 

     public function articlesecure(){
       return $this->belongsTo('App\Models\Article','article_id');
    }
}
