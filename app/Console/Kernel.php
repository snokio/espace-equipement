<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        
        $schedule->call('App\Http\Controllers\LogController@test')->dailyAt('12:05');
        // $schedule->call('App\Http\Controllers\Controller@TransfertCommande2')->daily();
        // $schedule->call('App\Http\Controllers\Controller@TransfertCommandePlanifie')->daily();
        // $schedule->call('App\Http\Controllers\Controller@TransfertAchats')->daily();
        // $schedule->call('App\Http\Controllers\Controller@TransfertAchatsPlanifie')->daily();
        // $schedule->call('App\Http\Controllers\Controller@TransfertStock2')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
