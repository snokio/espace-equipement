<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

// 1
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //2
        Validator::extend('airobo', function ($attribute, $value, $parameters, $validator) {
            $array = array_unique(str_split($value));
            if(count($array)>1){
                return true;
            }
        });
    }
}
